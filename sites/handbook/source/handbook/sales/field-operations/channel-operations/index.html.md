---
layout: markdown_page
title: "Channel Operations"
description: "This page serves as the Channel Operations team page and includes standard channel reporting links, and details for managing Channel opportunities"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


# Welcome to the Channel Operations page

## Standard Channel Reporting Links

**[Global Channel Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXTJ)**

*Use the Global Channel Dashboard to view Pipeline Creation, Open Pipeline, and Bookings reports grouped by Partner, Segment, or Region. Other reports within the Global Channel Dashboard include Deal Path, Engagement, Deal Type, Top Channel Deals by IACV, and Deals Registered through the Partner Portal.*

**[Global Channel Program Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXeR)**

**Reports by Territory**

<table>
  <tr>
   <td><strong>AMER</strong>
   </td>
   <td><strong>APAC</strong>
   </td>
   <td><strong>EMEA</strong>
   </td>
   <td><strong>PUB-SEC</strong>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRjJ">Channel Book & Pipe-Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRjO">Channel Book & Pipe-Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRjT">Channel Book & Pipe-Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRjY">Channel Book & Pipe-Current Fiscal Quarter</a>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRjd">Channel Book & Pipe by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRji">Channel Book & Pipe by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRjn">Channel Book & Pipe by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRjs">Channel Book & Pipe by Partner - Current Fiscal Quarter</a>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRif">Deal Path Pipe & Book - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRik">Deal Path Pipe & Book - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRip">Deal Path Pipe & Book - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRiu">Deal Path Pipe & Book - Current Fiscal Quarter</a>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRiz">Deal Path Pipe & Book - Current & Next Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRj4">Deal Path Pipe & Book - Current & Next Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRj9">Deal Path Pipe & Book - Current & Next Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRjE">Deal Path Pipe & Book - Current & Next Fiscal Quarter  </a>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRjx">Direct vs. Channel Book & Pipe - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRk2">Direct vs. Channel Book & Pipe - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRk7">Direct vs. Channel Book & Pipe - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRjt">Direct vs. Channel Book & Pipe - Current Fiscal Quarter  </a>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aS13">Open PIO Pipeline by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRhh">Open PIO Pipeline by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRiB">Open PIO Pipeline by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRiG">Open PIO Pipeline by Partner - Current Fiscal Quarter</a>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRiL">PIO Bookings by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRiQ">PIO Bookings by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRiV">PIO Bookings by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRia">PIO Bookings by Partner - Current Fiscal Quarter</a>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRhS">PIO Creation by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRhI">PIO Creation by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRhX ">PIO Creation by Partner - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRhc">PIO Creation by Partner - Current Fiscal Quarter</a>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRkW">PIO New Logos - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRkb">PIO New Logos - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRkg">PIO New Logos - Current Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aRkl">PIO New Logos - Current Fiscal Quarter</a>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aWXj">Channel Renewals - Current & Next Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aWXo">Channel Renewals - Current & Next Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aWXt">Channel Renewals - Current & Next Fiscal Quarter</a>
   </td>
   <td><a href="https://gitlab.my.salesforce.com/00O4M000004aUMP">Channel Renewals - Current & Next Fiscal Quarter</a>
   </td>
  </tr>
</table>

For a global view of current and next fiscal quarter channel renewals, [click here](https://gitlab.my.salesforce.com/00O4M000004aWY3).

*All required team reporting is included above. In the event you need a special report, please open an issue and tag Channel Ops.*

**[SFDC Channel Training Cheat Sheet](https://docs.google.com/document/d/1F1O3BUX80SJIqaFD9TF2RuyZ7DXB865AuiwaGctHnGo/edit?usp=sharing):** _Refer to this document for the training video hosted by the Channel Operations team, as well as quick reference "cheats" to help with your Salesforce reporting._

## Managing Channel Opportunities

### Policy and Process

All channel opportunities require a Partner to submit a Deal Registration via the Partner Portal in order to receive programmatic discounts. In the event that a Partner does not submit a Deal Registration (ex: Alliances, GSIs), but it is a Partner Sourced deal, the logic needs to match '`Initial Source = Channel Qualified Lead` and `Sales Qualified Source = Channel Generated` on the opportunity. For more details on the partner deal registration process and program go [here](https://about.gitlab.com/handbook/resellers/#the-deal-registration-program-overview).

_Note: The Partner Portal is Impartner which has SSO enabled with Vartopia which is the Partner facing Deal Registration system. Managed Services team are 3rd party resources that will help manage administrative processes for Deal Registration._

When a Partner submits a Deal Registration the Managed Services team will handle the initial submission. The submission will trigger the following actions:



1. Send an email to the Partner acknowledging the Deal Registration has successfully been created
2. Notify the Managed Services team to review
3. Create a record in SFDC on the Registration object with all of the details of the Deal Registration.

The Managed Services team will evaluate the Registration, link it to the appropriate Customer Account and Contact in SFDC and then send it to the applicable Channel Manager for approval/denial.

The Channel Managers need to review the deal registration and either approve or deny. It is highly recommended to work with the applicable GitLab Sales Rep prior to taking action.

Although multiple partners can submit a deal registration for an opportunity, only one deal registration can be approved for a specific opportunity. As a reminder, deal registrations are opportunity based and partners cannot register an account.

Before approving or denying the Deal Registration the Channel Manager needs to check to see if an Opportunity already exists.

4. Click the button at the top of the Deal Registration page:
![create_opp](/handbook/sales/field-operations/channel-operations/images/1-Link_Create_Opp_Screenshot.png)
5. ![approve_deny](/handbook/sales/field-operations/channel-operations/images/2-Approve_Deny_Opp_Screenshot.png)
If an Opportunity already exists then SELECT the applicable one.
If not then select:
![create_new](/handbook/sales/field-operations/channel-operations/images/3-Create_New.png)
If creating new, the applicable details from the Deal Registration will map to the Opportunity and automatically tag it as a Partner Sourced opportunity. Any other system required fields will also need to be filled out. Be sure to update the opportunity owner into the correct sales rep's name, and out of the channel manager's name.
6. After the Channel Manager either hits SELECT against a current Opportunity OR CREATE NEW, the Deal Registrations needs to either be approved or denied.
7. Click
![approve_deny](/handbook/sales/field-operations/channel-operations/images/4-Approve_Deny_Red.png)
   1. DENY: If denying the Deal Reg, the comments will be sent to the Partner along with updating the Deal Registration system and SFDC.
   2. APPROVE: If the Deal Registration is to be approved, then the Channel Manager will be required to select whether or not a Distributor is applicable to the deal if not already stated.
      1. If not, then no Distributor is required to be linked.
      2. If yes, then
      ![linked_disti](/handbook/sales/field-operations/channel-operations/images/5_Linked_Distributor.png)
      needs to be filled out before moving forward.
      If approved, then a notification will be sent to the Partner, the Deal Registration system updated, and the Registration status updated to Approved.

When a customer account is created by "User Vartopia" during a deal registration, that customer account should be reassigned to the appropriate sales rep. For more information, [click here](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement).

**Deal Registration System Status and Information:**

*   Deal Registration details will never override any information that the sales team forecasts on the Opportunity.
*   There is a Deal Registration ID that can be used to track all Deal Registrations in the system.
*   A Deal Registration is valid 90 days but can be extended if needed.
*   All Deal Registrations are now in their own object in SFDC under Registrations. They no longer come in as leads.


### Guidance for Deal Registration Processing

_Why is this important?_



*   Consistent and predictable partner experience
*   Efficient, simple, and objective approval process
*   Uphold integrity of deal reg reporting integrity
*   Mitigates liability
*   Reinforces our Transparency value

**    **





### Other Resources:
* Deal Registration Report [here](https://gitlab.my.salesforce.com/00O4M000004aUal)
* Partner Program [here](https://about.gitlab.com/handbook/resellers)
* Deal Registration Fields and Definitions [here](https://drive.google.com/file/d/1pdPHZpR_0sOUJlat-USGvcXfFjumDwC9/view?usp=sharing)
* Adding the Registration Object to your tabs [here](https://drive.google.com/file/d/1iUz42CfvYKPdw1quskc4NmraFVoZvFez/view?usp=sharing)
* Creating Personal Deal Registration Views [here](https://drive.google.com/file/d/1UtERcTNgNr9pTIf9OXDdpj3dNuhk9ISj/view?usp=sharing)
* Partner-Facing Step-by-step process [here](https://about.gitlab.com/handbook/resellers/#deal-registration-instructions)




### SFDC Field Definitions:

*   _DR - Partner:_ Partner that submitted the Deal Registration
*   _DR - Partner Deal Type:_
    *   _Resale:_ Partner is actually transacting the deal on their paper
    *   _Referral:_ Partner is bringing us the lead/opportunity but will either transact direct with GitLab or through another partner
    *   _Services Attach:_ Partner-delivered services provided to the end user related to their use of the GitLab software
*   _DR - Partner Engagement:_ How the deal was sourced or the value the partner is bringing
    *   _Partner Sourced:_ Partner has either found the original opportunity or it is an upsell to a current customer. If the `Initial Source = Channel Qualified Lead` or `Sales Qualified Source = Channel Generated', then the deal is Partner Sourced.
       - *In FY21, the Channel Team use "PIO" instead of Partner Sourced. The definition has been updated for FY22.
    *   _Assisted:_ GitLab-sourced opportunity where the partner assists our sales team in closing the deal
    *   _Fulfillment:_ Partner only processes the order and doesn’t provide additional support to close the deal
*   _Distributor:_ If the _DR - Partner_ are buying from a GitLab authorized distributor.
*   _Fulfillment Partner:_ Only applicable if the _DR - Partner Deal Type_ is "Referral" and the deal is being transacted through another partner.
*   _Platform Partner:_ Customer's platform that GitLab is being deployed.
*   _Influence Partner:_ Other partners, generally GSIs or alliances that have helped influence a deal.


_For more details on Partner Engagement definitions go [here.](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#program-and-incentive-definitions)_


### Rules of Engagement on Channel Deals

*   Deal registration approval is based upon order of receipt of the registration, qualification of the opportunity, partner ability to deliver in-country/region support, and partner relationship with customer. Final deal registration approval decision will be made by GitLab Channel after discussion with GitLab Sales.
*   Only one partner can earn a deal registration discount per opportunity. Partners, other than the partner granted the deal registration discount that requests a quote, will receive the fulfillment discount rate.
*   Any partner opportunity can be a registered deal.  These opportunities can be either Partner Sourced, Partner Assist, Partner Fulfilled or Partner Services Attach.  Visit [Program and Incentive Definitions](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#program-and-incentive-definitions) to learn what deals qualify for each category.
*   Approved deal registrations have standard 90-day expiration from the date of original approval (Deal Registration extensions beyond the initial 90 days approval are at the sole discretion of GitLab).
*   GitLab collaborates with partners holding the approved deal registration and is available to support partners throughout the entire sales process.
*   In the event the engagement is dissolved, the GitLab Sales Rep will generally notify the partner by phone or email. GitLab will reconsider other qualified deal registrations submitted for this deal, in chronological order of submission. If there are no other registration requests submitted, the GitLab sales rep will typically initiate engagement with a reseller of the GitLab sales rep’s choosing.


### Quoting Requirements for Channel Deals

Any time a deal is being transacted via the Channel, a GitLab quote is **<span style="text-decoration:underline;">required</span>** to be created in SFDC if any pricing is being offered other than MSRP (List Price).

At a minimum, a [Draft Proposal](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#how-to-create-a-draft-proposal) needs to be created and provided to the Partner. If a Partner has an approved Deal Registration, then an approved quote needs to be created and provided to that Partner.

Discounted quotes that are not in the system and sent to a Partner are not permitted at this time. This includes providing product and pricing quotes details in email. This applies to all GEO’s and Segments.

Any questions or issues, please reach out to either Channel-Ops or Deal Desk.




## Managing Special Cases

### Creating MSP Opportunities

A Managed Service Provider (MSP) purchases licenses on behalf of an end user. The MSP will be the owner and manager of the licenses but their customer - the end user - is the one using the licenses. This creates specific needs in GitLab Salesforce opportunities to ensure proper reporting and compensation.

When you have an MSP opportunity, the Sales Reps need to follow these ***additional*** steps:

**Step 1:** The opportunity must be created using the MSP partner account, NOT the potential customer on whose behalf they are purchasing.

**Step 2:** Change the opportunity owner to the correct Sales Rep that owns the end-user account even though the opportunity is created under the Partner MSP account.

**Step 3:** Fill out the Partner and Deal Registration Information Section per the following:
- DR-Partner: this must list the MSP’s _Partner_ account (same as the opportunity is created under)
- DR-Deal type: "MSP"
- DR-Engagement: Select applicable answer
  - _If there is an approved Deal Registration, the Partner data will automatically be added when the Deal Registration is linked to the Opportunity. The DR-Engagement will be the only piece that needs to be filled out._

**Step 4:** When filling out the quote for this opportunity, select the **MSP quote template**.
- Invoice owner: The MSP Partner (same as DR-Partner referenced above).
- Special Terms and Notes: Name of the End-user Customer (the MSP owns the licenses on behalf of).

### Creating a Service-Attached Opportunity

_New Process as of November 3rd 2020_

A Service-Attached Deal Registration needs to be created to track when a partner offers their own professional services along with GitLab licenses. This is separate from the Deal Registration for the license sale.

To track the Partner Services, the partner must register the deal on the [partner portal](https://about.gitlab.com/handbook/resellers/#gitlab-partner-portal).

Please follow the above steps 1 - 4 [Deal Registration Process](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#policy-and-process)

Next, select the Opportunity that is applicable to the GitLab sales of licenses that the services are being performed for. This GitLab sale opportunity may be open or closed won.

This Services Deal Registration is now attached to the GitLab sale opp. Once linked, the Channel Manager will now need to approve per the Deal Registration by selecting

![approve_deny](/handbook/sales/field-operations/channel-operations/images/4-Approve_Deny_Red.png)
.

Once approved then a notification will be sent to the Partner, the Deal Registration system updated, and the Registration status updated to Approved.

Rebate payouts will be reported and paid after each GitLab quarter close.


_Prior to November 3, 2020_

A Service-Attached opportunity is created to track when a partner offers their own professional services along with GitLab licenses. This is separate from the license sale and respective Salesforce opportunity.

To create the opportunity, the partner must register the deal on the [partner portal](https://about.gitlab.com/handbook/resellers/#gitlab-partner-portal). **The opportunity needs to be created for the service-attach opportunity alone.** The service-attached opportunity should never be a part of, or added on to the GitLab product sale opportunity.

For proper reporting, ensure that all the correct fields are used to notate that this is a partner service-attached opportunity.
- DR - Partner: The partner account providing the services
- DR - Partner Deal type: Services Attach
- DR - Partner Engagement: N/A

GitLab Sales needs to add the GitLab product sales opportunity as the parent opportunity to the service-attached opportunity.

As the opportunity progresses, use the stages of the opportunity (0 - 7) until the agreement documentation is received that the customer is moving forward. Once the agreement for the Partner Services is completed (i.e. SOW, quote, order form), change the opportunity to **Stage: 10-Duplicate.**

### Multiple-bid Process

For opportunities where there are multiple partners bidding on the same opportunity, it’s important that each partner gets the appropriate pricing for the opportunity.
- The partner with the approved Deal Registration needs to receive the [documented discount for the program](https://gitlab.my.salesforce.com/0694M000008xAk4).
- **All other partners quoting/bidding on the opportunity do not receive any partner discounts. They should be provided MSRP only.**
- If the deal includes a distribution partner, that distributor receives their contracted margin.

For more information on quoting or the Partner Program, please visit:

[Deal Desk Quote Configuration](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#zuora-quote-configuration-guide---standard-quotes)

[Partner Program](https://about.gitlab.com/handbook/resellers/)

## Channel Reporting and Tagging
![Reporting_Tagging_Matrix](/handbook/sales/field-operations/channel-operations/images/Reporting_Tagging_Matrix.png)

**Definitions:**
1. _Deal Path_: How the deal is transacted. Values can be Channel, Direct, or Web. Also, includes Referral Ops for Channel. 
2. _Deal Reg_: Partner submits a Registration for their opportunity via the Partner Portal. For the purposes of this matrix the assumption is the Deal Reg is approved
3. _Initial Source_: SFDC Lead value that is populated based on lead source. Will default to CQL when a Partner submits a Deal Reg and an Opportunity does not already exist in the system. 
4. _SQS_: Who converts/creates the Opportunity in SFDC. **Can only be 1 value**
5. _DR - Partner Engagement_: Partner Value on the deal via the Partner Program definitions. This is manually selected in most cases
6. _Order Type_: Customer order designation in SFDC. New First Order or Growth

**Use Cases:**
1. **#1 and #3**
   - Channel submits Deal Reg and no Opportunity exists in the system. Therefore the Initial source is CQL,  and SQS and DR-Partner Engagement default to Channel and Partner Sourced
      - This applies to both New and Growth orders
2. **#2 and #4**
   **- Examples:**
      - AE Creates Opportunity prior to Deal Reg being submitted  - **CAM to escalate for exception**
      - Opportunity stalled and Channel helps to drive to closure - **If channel is simply unsticking an open opp then this is technically Assist. Exceptions can be reviewed**
      - Aged opportunities that are closed and revived due to Channel - **Automated clean up with Sales Ops stale Opp policy**
   **- Exception:** In the event an exception is needed per the scenarios below and exception can be submitted for review and have the ability “override” and restate these are Channel SQS


**Default Logic**
1. Deal Reg = True and no Opp Exists then Initial Source = CQL > SQS = Channel, Defaults to Partner Sourced
2. Alliances: Does not have same logic and will need to be reported separately currently



## SFDC Opportunity Source Field Values for Channel

**SFDC Opportunity Fields**:

-  _Initial Source:_
   - **Channel Qualified Lead (CQL):** GitLab Channel Partner created and/or qualified the Lead whether they sourced it themselves or GitLab provided the inquiry to them to work
-  _Sales Qualified Source:_
   - **Channel:** Channel Partner has converted the Lead/CQL to a Qualified Opportunity. This field defaults to Channel when Initial Source = _CQL_
- _DR - Deal Engagement:_
   - **Partner Sourced:** Partner has either found the original opportunity or it is an upsell to a current customer. If the `Initial Source = Channel Qualified Lead` or `Sales Qualified Source = Channel Generated`, then the deal is Partner Sourced.
   - **Assist:** Partner Assisted Opportunity
   - **Fulfillment:** Partner Fulfillment Opportunity

*For additional definition and qualification of Deal Engagement type go [here.](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#program-and-incentive-definitions)

**For additional Source definition please visit the [Marketing Handbook Page. ](https://about.gitlab.com/handbook/marketing/marketing-operations/#initial-source)


##  Program and Incentive Definitions

### Partner Program Discounts
- The GitLab Partner Program provides partners with set discounts based on their engagement in opportunities (see definitions below) and their partner program track.
- Partners are not eligible for discounts on sales or renewals of Bronze / Starter licenses.
- GitLab employees can access the discount table [here.](https://gitlab.my.salesforce.com/0694M000008xAk4)  Partners can find the discount table in the Asset Library on the GitLab Partner Portal.

### Partner Sourced Opportunity
- A Partner Sourced opportunity is an opportunity where a Partner has either found an original opportunity or is upselling a current customer. 
- An opportunity can only be Partner Sourced if the `Initial Source = Channel Qualified Lead` or `Sales Qualified Source = Channel Generated`.
- A Partner Sourced opportunity could be:
    -  An opportunity with new customer to GitLab
    -  An opportunity with a customer/prospect with which GitLab is already engaged, but was not aware of the specific Partner Sourced opportunity
    -  An existing customer upgrade to a higher pricing plan. This could be for a customer that was originally sold by GitLab.
    - Additional licenses sold, often at the time of renewal.  This can also be an increase in size of an existing GitLab sales opportunity.
    -  True-ups to an original partner opportunity.
- The opportunity must be new to our sales team or added to an existing sales opportunity, and can be for a new or existing customer.
- The partner is expected to collaborate with the GitLab Sales team to jointly close the sale.
- For US Public Sector, each unique customer opportunity within a single government program can be Partner Sourced.
- For resale, the partner receives an upfront discount that is dependent on the partners track within the GitLab Partner Program.
- Referral rebate payments are paid out no later than 45 after the end of each GitLab fiscal quarter.
- The determination of Partner Sourced is per the system logic stated above and tracked via Salesforce opportunities.


### Partner Assist Opportunity
- Any opportunity where the partner assists our sales team in closing the deal.
- This assistance may include any or all of the following: a customer demo/POV, an executive introduction meeting, delivery of services, helping with the transaction, financing.  Often this is leveraging the partner's incumency.
- Partners need to submit a Deal Registration for Partner Assist.  Since it is for a GitLab sourced opportunity, it does not qualify to be Partner Sourced, but should be tagged as Partner Assist in Salesforce.
- The determination of Partner Assist is at the sales rep & CAM determination and tracked via SFDC opportunities.

### Partner Fulfill Opportunity
- Any opportunity that was fulfilled by a partner but closed independently via the GitLab sales team.
- The partner has only processed the order and didn’t provide any meaningful support to close the deal.

### Services Attach Opportunity
- Any partner delivered services that are provided to the end user in support of a GitLab deployment.
- This will result in a 2.5% upfront discount from the product for resales opportunities and a rebate if partner is not involved in resale.
- The resale discount will be administered as an upfront discount from the GitLab license price on the most recent product sale net license price.
- This is stackable for up to three (3) independent services engagements over a twelve (12) month period, provided by the partner to a single end user.
- The maximum discount or rebate is 7.5% with any combination of upfront or post sales partner branded services.
- Partner Services engagements must meet the following partner services engagement deal size minimums to qualify:
    -  1st deal: => $7,500 in services
    -  2nd deal: =>$10,000 in services
    -  3rd deal: => $10,000 in services
- Partners must register a Services Attach deal registration and provide proof of performance to qualify for the incentive.
- Rebate payments are paid out at the end of each GitLab fiscal quarter.
- Rebates and referral fees may require CRO approval.

### Services Resale
- Partners can earn a program discount for reselling GitLab Professional Services delivered services.
- To qualify for the Services discount, the services must be included on the order of a deal registered opportunity.

### Incumbency Renewals
- Incumbent partners qualify for program renewal discounts.  The partners that transacts the most recent sale (IACV) are considered the incumbent
- A different partner can earn an incumbency discount only through formal written communications from the customer.  This can be provided via email from an authorized representative from the customer.
- In some cases, a customer purchased their most recent subscription directly from GitLab, but is directed to a partner for renewal. Partners are encouraged to engage with these customers, but their first renewal of a formerly direct customer will not be discounted for partners.
- To earn partner discounts, partners will be required to meet compliance requirements (i.e. in good credit standing, have provided quarterly updates on customer, review within 30 days of renewal, etc).

### Tender Offers
- Tender offers are ones where the customers are requesting multiple bids for a project.
- Each partner bidding should register the deal. Since all partners would be engaged in the sales process and would be creating a bid, all partners qualify for Partner Assist discount (% based on their program track). If the first partner registering the deal was early in with the customer (pre-tender), introduces the opportunity to GitLab, and the appropriate system logic fits, that partner could earn a Partner Sourced discount. If the partner earning the Partner Sourced discount is not awarded the deal, they would not receive additional referral compensation.
Any partner offering services would qualify for Services-Attach incentives in addition to any sales discounts for which they would qualify.

### Program Compliance
- For partners to transact according to program discounts, they must agree to the GitLab Partner Agreement.
- To earn partner discounts, partners will be required to be program compliant.
- Non Contracted partners may transact on a one-off basis, only with approval of channel leadership.

### Unauthorized Partners
- Unauthorized partners are ones that have not signed a GitLab Partner Agreement.
- A key goal of the GitLab Channel Program is the success of our authorized partners. We are developing our channel to provide coverage across geos, customer segments and vertical markets. Since the program was just launched in April 2020, we have not onboarded enough partners to support every sales opportunity. As we continue to build out our channel coverage, there will still be a need to utilize one-off, unauthorized partners for specific sales opportunities.
- For Developed Regions - Most P0 and many P1 countries.  GitLab Sales teams, work with your CAM to identify authorized partners in your region.
GitLab Sales teams should use existing, authorized GitLab partners, including our distributors, whenever possible.
For FY21 Q2 and Q3, one-off partners can provide fulfillment transactions utilizing the Fulfillment contract.  Additional instructions for opportunities sold via fulfillment partners are available in the Handbook.
Q4 and beyond - VP approval required for one-off partner requests.
- For Developing Regions - GitLab Sales teams, work with your CAM to identify authorized partners in your region.
Use fulfillment contract for one-off partner deals
On a quarterly basis, the Channel team will revisit developing countries to determine if there is a continued need for fulfillment partners, or if we have the necessary coverage with authorized partners.



## Partner Applicant Approval / Denial - Granting Portal Access

Partner Program participation sign ups must be initiated by the Partner in the Partner Portal application form which can be found [here.](https://rv.treehousei.com/en/login.aspx)  In the partner application process, channel partners review the partner contract, including both the resale and referral addenda, review the partner program guide, complete their application form and agree to program terms and conditions.   Technology partners are not able to agree to the terms and conditions during the application process.

 If an authorized representative of the channel partner accepts the agreement terms, they (or the person entering the application) will need to select “Yes” that they agree to terms on the application form.  Once they have agreed, they will automatically be set to “ Authorized” and will get immediate access to the partner portal.  At this time, partners will be set up in both Salesforce and the partner portal at Authorized and their track set to Open.

The partner will receive an email confirming the receipt of their application, and applicable Channel Sales or Alliance Manager will receive a New Partner notification email from Partnersupport@gitlab.com notifying there is a new partner applicant in that region.  Channel Sales Managers will be notified of each partner application in their regions, whether they agreed to the terms or not.

Upon receiving notification they will be responsible for reviewing the partner’s information and deactivating any inappropriate partners.  They will also need to set the Partner Type in Salesforce for newly authorized partners.

For partners that have questions about the contract or need to negotiate terms and conditions, Channel Sales Managers are responsible for working with the partner offline to address questions and come to agreement on program terms.  Upon receiving the New Partner Applicant notification email, the applicable Channels Sales Manager needs to complete the following:

1. Contact the partner and qualify them
2. If the decision is to move forward with the partner first check to see if a partner account already exists in Salesforce. If it is a duplicate, request for the accounts to be merged by the Channel Operations team. If the decision is to deny the partner then go to step #7.
3. To start the contracting process click the Legal Request button in SFDC on the partner account record.
    *   Request the appropriate contract addendum (Resale, Referral/Services or both **OR** MSP **OR** OTHER). Default should be Resale and Referral/Services.
4. Once the contract is fully executed and attached to the partner account record in SFDC the following fields need to be updated by the Channel Sales Manager and are required(*) in order to save the account
    *   *Change Partner Status = Authorized
    *   *Select Partner Type
    *   For partners that signed standard contract terms, set Partner Program Status to “New”.
    *   Please update the partner record to be as complete as possible.
    *   For additional information on the Partner Program review [here](https://about.gitlab.com/handbook/resellers/#partner-program-tracks)
5. Once a partner is authorized, each SFDC contact for that partner will automatically receive a message with login credentials to the portal.
6. Additional partner employees can go to partners.gitlab.com to register. \ Once they are linked to an authorized partner account (they must select the correct account upon registering), they will automatically receive a message with login credentials. If the account is still a Prospect they will not have access until the account has an executed contract and is moved to Authorized.
7. If the decision is to not move forward with the partner,
    *   Channel Sales Manager needs to set Partner Status = Denied

Technology partners use the same form, but are not able to agree to the terms and conditions.  Once they submit the form, they will be set to active.  If the Alliances team wants to establish a contract with the partner, they must follow the Legal Request process in Salesforce.

If for any reason, a partner account needs to be created in Salesforce directly, requests for account creation can be made to #channel-ops within Slack.

Visit the [Partner Applicant / Partner Portal FAQ](https://docs.google.com/document/d/1aPCqF5-qb2XxFEhvkNzvexwsIYGuiJF8AhK_qeUgw0Y/edit?usp=sharing) for additional information.



## Alliances and OEMs

[Alliances Salesforce Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYAp)

### Opportunity Tagging for Google Cloud and Amazon Web Services Deals
If a deal is being transacted through **GCP  Marketplace** or **AWS Marketplace**, then the following fields needs to be filled out on the Opportunity:
 - **DR - Partner** should be filled out using these SFDC accounts:
   - [https://gitlab.my.salesforce.com/0014M00001nJhks](https://gitlab.my.salesforce.com/0014M00001nJhks)
   - [https://gitlab.my.salesforce.com/0014M00001ldTdt](https://gitlab.my.salesforce.com/0014M00001ldTdt) 
 - **DR - Partner Deal Type** = Resale
 - **DR - Partner Deal Type** = Partner Sourced, Assist, or Fulfillment

If Google or AWS has brought us a lead/referred us a deal but will not be transacting on their Marketplace, then the following fields should be filled out on the Opportunity:
 - **DR - Partner** should be filled out using these SFDC accounts:
   - [https://gitlab.my.salesforce.com/0014M00001nJhks](https://gitlab.my.salesforce.com/0014M00001nJhks)
   - [https://gitlab.my.salesforce.com/0014M00001ldTdt](https://gitlab.my.salesforce.com/0014M00001ldTdt) 
 - **DR - Partner Deal Type** = Referral
 - **DR - Partner Deal Type** = Partner Sourced

If Google or AWS has assisted on a deal and helped drive the customer to buy GitLab, but was not the original source of the opportunity, then the following fields should be filled out on the Opportunity:
 - **Influence Partner** = should be filled out using these SFDC accounts:
   - [https://gitlab.my.salesforce.com/0014M00001nJhks](https://gitlab.my.salesforce.com/0014M00001nJhks)
   - [https://gitlab.my.salesforce.com/0014M00001ldTdt](https://gitlab.my.salesforce.com/0014M00001ldTdt) 

### Requesting Google Cloud Credits:
Required fields when requesting Google Cloud Credits on an Opportunity
1. Have you engaged with the GCP Team already? (Drop down: Yes, No)
2. Customer open to being a reference? (drop down: logo use, case-study, joint speaking session, etc.)
3. Credits being requested (Sales Rep enters in the amount of credits required to close the deal)

Once all required information is provided, it will be routed internally for approval. 


## Channel Neutral

Comp Neutrality applies to all GitLab Opportunities where a Partner is involved in the physical  transaction (takes paper) **<span style="text-decoration:underline;">and</span>** a Partner Program discount is applied on the executed Partner Order Form.

The maximum Comp Neutral payout is based on the applicable GitLab Partner Program discount matrix. If additional discount is needed in order to close the deal, that portion is not eligible for Comp Neutrality.

In the event a lesser discount is applied on the deal then what the Partner Program allocates, then the lesser discount will also be what is calculated for Comp Neutrality.

As a reminder, comp neutrality only applies to comp and does not retire quota. For additional information, please review [Channel Neutral Compensation](https://about.gitlab.com/handbook/sales/commissions/#channel-neutral-compensation) within the Sales Ops commission section of the Handbook.

Internal: [Partner Program Discount Matrix](https://gitlab.my.salesforce.com/sfc/p/61000000JNOF/a/4M000000g8Ba/fMhmeefW.jeiHK3_Hq5iANC3ogdHk9N3j0MgdWIpmI4)

For Partner Program discounts for PubSec, please reach out to #channel-ops on Slack.

### Comp Neutral Calculation in SFDC

In the event the opportunity is going thru a Partner and therefore qualifies for CN, then the appropriate partner information must be filled out on the Opportunity and Quote in order for the CN field to calculate properly.

Required Partner Fields on the Opp:
*   DR - Partner
*   DR - Partner Deal Type
*   DR - Partner Engagement
*   Distributor _(as applicable)_

Required Partner Info on the Quote
*   Invoice owner contact and Invoice owner Type must be a Partner Account and Contact
*   Order Form must be a Partner Order Form

**Example Calculation No. 1:**

```
Partner Track               : Select
Product                     : Standard (Previously Premium)
QTY                         : 25
Term                        : 1 year
Partner Deal Type           : Resale
Partner Engagement          : Partner Sourced
Deal Registration           : Approved
Partner Program Discount    : 15%
```

- $228 x 25 x 1 = $5700 (price without program discount aka MSRP or List Price)
- $228 x 25 x 1 x 15% = $4845 (quote/order form price with discount)
- $5700 x 15% = $855 (comp neutral amount)


**Example Calculation No. 2 (PubSec):**

```
Partner Track               : Open
Distributor                 : True
Product                     : Ultimate
QTY                         : 50
Term                        : 1 year
Partner Deal Type           : Resale
Partner Engagement          : Assist
Deal Registration           : Approved
Partner Program Discount    : 10%
```

- $1188 x 50 x 1 = $59400 (price without program discount)
- $1188 x 50 x 1 x 15% = $50490 (quote/order form price with discount)
- $59400 x 15% = $8910 (comp neutral amount)

### 2-Tier Opportunities

If the Opportunity is going through a 2-Tier Channel (Distributor + Reseller), both Partner Accounts must be included on the Opportunity in the Partner Information section in order for both Program discounts to apply AND for Comp Neutral to calculate on both. Reseller should be in the DR - Partner field and the applicable Distributor in the Distributor field on the Opportunity.

### Opportunities with GCP and AWS

For deals going through GCP and AWS, the Partner fields should still be filled out on the Opportunity but comp neutral will not calculate until the deal closes as partner discounts are done after the quote/order form is generated. Deal Desk will assist with this process.

## Frequently Asked Questions

**Where can I find the Standard Channel Discounts for my Partners?**
- Follow [SFDC Discounting table](https://gitlab.my.salesforce.com/0694M000008xAk4)

[Public Sector discount table](https://gitlab.my.salesforce.com/0694M000009YhAcQAK)

**Where can I find more information about our current Partner processes?**
- The current Resellers Handbook can be found [Here](/handbook/resellers/)

**Whats the current Deal Registration Process?**
- The current process can be found [Here](/handbook/resellers/#deal-registration)

**How do I track the opportunity of a partner deal?**
Please fill in at least one of the following Opportunity drop downs to identify the deal as a Channel opportunity.
- **If the partner registered the deal** the `Deal Registrar`field will need to be populated in the SFDC opportunity with the appropriate Partner Account.
- **If the Partner Assisted you** (see above for definition) please populate the `Partner - Assisted` field in the SFDC opportunity with the appropriate partner account.
- **If GitLab sourced and sold the deal directly, and the partner is simply fulfilling the deal**, please populate the `Partner - Fulfillment` field in the SFDC opportunity.

**How do I check what I'm being paid on?**

**What are some examples of Channel Neutral math?**

| **Deal Calculation** | **Direct Deal** | **Channel 1 (Neutral)** | **Channel 2 (Add’l Disc)** | **Channel 3 (Split Disc)** |
|----- | ----- | ------ | ------ | ------|
| List Price | $100,000 |  $100,000 | $100,000  | $100,000 |
| Channel Discount | 0% | 20% | 20% | 20% |
| Additional Rep Discount | 5% | 0% | +5% | +2.5% |
| **Total Discount** | **5%** | **20%** | **25%** | **22.5%** |
| IACV (Quota Relief) | $95,000 |  $80,000 | $75,000  | $77,500 |
| Channel Neutral (No Quota Relief @ BCR)  | +$0 |  +$20,000 | +$20,000  | +$20,000 |
| **Commissionable Amount** | **$95,000** |  **$100,000** | **$95,000**  | **$97,500** |
| Base Commission Rate (BCR) | % 8 |  % 8 | % 8  | % 8 |
| **Commission Payout** | **$7,600** | **$8,000** | **$7,600**  | **$7,800** |


**How do I get Channel deals/discount approvals?**
Follow [standard approval process](/handbook/business-ops/order-processing/#step-5---submitting-a-quote-for-discount-and-terms-approval) in SFDC

**How does the Amazon Process work?**
Follow [Amazon Web Services (AWS) Private Offer Transactions](/handbook/business-ops/order-processing/#amazon-web-services-aws-private-offer-transactions) handbook

**I need HELP!  How to do I reach out to the experts?**

The quickest way to get help is by using the following Slack channels:
- #channel-sales
- #channel-ops
- #alliances

