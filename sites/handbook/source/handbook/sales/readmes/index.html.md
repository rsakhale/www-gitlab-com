---
layout: handbook-page-toc
title: "GitLab Sales Team READMEs"
---

## Sales Team READMEs

- [Tim Poffenbargers's README (Solutions Architect)](/handbook/sales/readmes/tim-poffenbarger.html)
- [Noria Aidam's README (Sales Developement Representative Enterprise)](/handbook/sales/readmes/noria_aidam.html)
- [Chris Cowell's README (Senior Professional Services Technical Instructor)](/handbook/sales/readmes/chris-cowell/)
