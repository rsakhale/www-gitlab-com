---
layout: handbook-page-toc
title: "Projects"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

We maintain our projects in the [gitlab-de](https://gitlab.com/gitlab-de) group. This includes repositories for open source projects, workshops exercises, and more learning resources.

* [Docker Hub Limit Monitoring](/blog/2020/11/18/docker-hub-rate-limit-monitoring/)
    * [Exporter for Prometheus](https://gitlab.com/gitlab-de/docker-hub-limit-exporter)
    * [Monitoring Plugin](https://gitlab.com/gitlab-de/check-docker-hub-limit)
* [Go Excusegen](https://gitlab.com/gitlab-de/go-excusegen)
* [CI Community Day 2020](https://gitlab.com/gitlab-de/ci-community-day-2020)
* [1. Swiss Meetup 2021](https://gitlab.com/gitlab-de/swiss-meetup-2021-jan)

This page provides more insights into projects run or influenced by Developer Evangelists.

## everyonecancontribute cafe

Maintainer: [Michael Friedrich](/company/team/#dnsmichi)

[everyonecancontribute.com](https://everyonecancontribute.com) serves as the main website for a community formed around tech coffee chats called `#everyonecancontribute cafe` and `#everyonecancontribute Kaeffchen`.

The weekly coffee chats come with a theme and allow to

* Try out newly announced projects together.
* Do pair programming/debugging sessions.
* Start discussions and share ideas on the latest technology.

The sessions are hosted by Michael Friedrich with their Zoom and Calendar invites. The website's [About page](https://everyonecancontribute.com//page/about/) covers more details including the exact date and time.

Insights:

* The website is built using [GitLab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).
* Organization happens in the GitLab group [everyonecancontribute](https://gitlab.com/groups/everyonecancontribute/-/issues). This group has applied for an Ultimate license for OSS projects.
* [Gitter](https://gitter.im/everyonecancontribute) channel.
* [#everyonecancontribute cafe (English)](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp1Gni9SyudMmXmBJIp7rIc) YouTube playlist.
* [#everyonecancontribute Kaeffchen (German)](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko8J8V5V794CXZUZ-DLxccI)YouTube playlist.

[everyonecancontribute.dev](https://everyonecancontribute.dev) hosts a demo page with funny animations featuring the Tanuki, Clippy, and more. It is deployed in a container environment and provides a Prometheus node exporter for monitoring demos and talks. Michael created the website for the job application presentation panel at GitLab.

### everyonecancontribute cafe groups

- [Kubernetes](https://gitlab.com/everyonecancontribute/kubernetes) with hands-on workshop series.
- [Keptn, GitLab, Prometheus](https://gitlab.com/everyonecancontribute/keptn) to collaborate on a deeper integration.
- [5minprod.app](https://gitlab.com/everyonecancontribute/5-min-prod-app) to drive the [5 minute production app](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template) with community resources.


## Developer Evangelism Dashboard

Maintainer: [Abubakar Siddiq Ango](/company/team/#abuango)

Our [custom dashboard](https://gitlab-com.gitlab.io/marketing/corporate_marketing/technical-evangelism/code/de-dashboard/) is built using [GitLab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) and presents an overview of Developer Evangelism issues. The requests are synced in a specified interval.

Project: [DE Dashboard](https://gitlab.com/gitlab-com/marketing/corporate_marketing/technical-evangelism/code/de-dashboard)

## Developer Evangelism Bot

Maintainer: [Abubakar Siddiq Ango](/company/team/#abuango)

Similar to the triage bot, this bot aims to automate DE Tasks such as:

* Pull social media metrics from team members into a defined spreadsheet
* Create release evangelism issues for team members on the 15th every month.
* Generate an issue letter (created, closed, open CFPs) on every Monday.

Project: [DE Bot](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/code/de-bot)

## Lab Work

Maintainer: [Brendan O'Leary](/company/team/#brendan)

[labwork.dev](https://labwork.dev/) is a collection of applications made to show off exciting ideas and development challenges.

* Link Shortener
* [15th Anniversary of Git](https://git15.labwork.dev/)

Project: [Lab Work](https://gitlab.com/brendan/labwork)

## Events Project

Maintainer: [Abubakar Siddiq Ango](/company/team/#abuango)

The Developer Evangelism Team at GitLab tracks events happening across the industry for attendance, speaking engagements, or just to keep them on the radar. This is also a major need across the industry. The Events project aims to provide an open and collaborative place where members of the wider GitLab community can add and track events happening in the industry.

Project: [DE Events Project](https://gitlab.com/gitlab-de/events)

## Our Work Environments

* [Brendan's dotfiles](https://gitlab.com/brendan/dotfiles)
* [Michael's dotfiles](https://gitlab.com/dnsmichi/dotfiles) covered in [this blog post](https://about.gitlab.com/blog/2020/04/17/dotfiles-document-and-automate-your-macbook-setup/)
