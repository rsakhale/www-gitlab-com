---
layout: handbook-page-toc
title: "Open Source Program"
description: Learn about the GitLab for Open Source program and other programs run by GitLab's Community Relations team.
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

# <i class="far fa-newspaper" id="biz-tech-icons"></i>  About
GitLab's open source program is part of the [Community Relations team](/handbook/marketing/community-relations/). 

On this page, you'll find information about the following sub-programs that are part of the open source program at GitLab. You'll find information about what they are and how they are run.

| Program Name | Description | Link to place in handbook |
| ------ | ------ | ------ |
| [GitLab for Open Source Program](/solutions/open-source/) | Qualifying open source projects get our top tiers and 50K CI minutes for free. Applications are processed according to the [Community Programs Application Workflow](/handbook/marketing/community-relations/community-operations/community-program-applications/). | [Section 1](/handbook/marketing/community-relations/opensource-program/#gitlab-for-open-source-program) |
| [Open Source Partners](/solutions/open-source/partners) | A partnership program designed for large or prominent open source orgs | [Section 2](/handbook/marketing/community-relations/opensource-program/#gitlab-for-open-source-partners) |
| [Consortium Memberships](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships/-/boards) | These memberships allow us to show our technology thought leadership, help with our branding, and/or provide opportunities for engineering alignment | [Section 3](/handbook/marketing/community-relations/opensource-program/#consortium-memberships) |

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> How to reach us
 * DRI: Nuritzi Sanchez - `@nuritzi` on GitLab
 * Slack channel: #education-oss
 * Email: opensource@gitlab.com

## <i class="fas fa-tasks" id="biz-tech-icons"></i> Where to find what we're working on
Epics, issue boards, and labels are used to track our work. For more information on the specific ones we use, please visit the [Community Relations project management page](/handbook/marketing/community-relations/project-management/).

### OKRs (goals for current quarter)
GitLab creates [Objectives and Key Results (OKRs)](https://en.wikipedia.org/wiki/OKR) per quarter. Here are the [current OKRs for the GitLab for Open Source program](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=OSS%20OKR).

### Metrics

We use the [GitLab for Open Source KPI Dashboard](https://app.periscopedata.com/app/gitlab/670411/Open-Source-Program) on Sisene to track program metrics. This dashboard's visibility is internal to GitLab team members only and measures the following: 

**Current status stats:** 
These stats give us an idea of how many active users we have as part of the GitLab for Open Source program.
 * **Active projects** - number of program members that have an active new application or renewal.
 * **Active seats** - number of users under the GitLab for Open Source program. Since we do not enable Telemetry, this number is taken from the number of seats program members request.

**Stats counted from program inception:**
Lifetime stats of how the program has performed since its creation. 
 * **Number of licenses** - these are licenses issued since the beginning of the program (includes add-ons, and renewals).
 * **Enrolled projects** - how many unique projects have enrolled since the program began.
 * **Projects renewed** - the percentage of projects that have renewed since program began.
 * **Seats issued** - total seats issued since beginning of program.

**Quarterly stats:**
 * New projects enrolled per quarter
 * Number of seats granted per quarter

# <i class="far fa-newspaper" id="biz-tech-icons"></i> GitLab for Open Source Program

## Overview
*At GitLab, our mission is to change all creative work from read-only to read-write so that everyone can contribute.*

The [GitLab for Open Source program](/solutions/open-source/) supports GitLab's mission by empowering and enabling open source projects with our most advanced features so that they can create greater impact and amplify the contribution mindset within their spheres of influence.

This program's vision is to make GitLab the best place for Open Source projects to thrive at scale.

Apart from actively maintaining and constantly adding value to the open source [Community Edition](/install/ce-or-ee/), GitLab makes the Enterprise Edition's top [Ultimate](/pricing/#self-managed) and [Gold](/pricing/#gitlab-com) tiers available free-of-cost to qualifying open source projects.

### What's included?
- GitLab's top tiers ([self-hosted Ultimate and cloud-hosted Gold](/pricing/)) are free for open source projects
- 50,000 CI minutes are included for free for GitLab Gold users. Additional CI minutes can be purchased ($8 one-time fee for 1,000 extra CI minutes)
- Support can be purchased at a discount of 95% off, at $4.95 per user per month

More information about the program can be found on the [GitLab for Open Source landing page](/solutions/open-source/).

Note: This program grants Gold features are the _group_ level (these features are things like epics, roadmaps, merge requests, and other Gold features for groups). Any public project on GitLab.com automatically gets Gold features at the _project_ level. 

#### Support, add-ons, and additional services
**Priority Support.**
While our GitLab for Open Source program does not include paid support, members of the program receive a 95% discount on [Priority Support](/support/). Support can be purchased for $4.95 per user per month by sending us an email to opensource@gitlab.com and requesting an add-on.

**Migration Services.**
For open source communities that need assistance with their migration, our team provides a variety of services to help. Check out our [professional services](https://about.gitlab.com/services/) page for more information.

**Additional services.**
We sometimes hear requests for services we do not provide, such as providing hosting for Community Edition instances. We've partnered with others to help expand the options for our users. Check out our [Technology Partners](/partners/technology-partners/) to learn more.

### OSS Product SKUs
The following SKUs are related to the GitLab for Open Source program:

 * Gold OSS - 1 Year
 * Gold OSS - 1 Year w/ Support
 * Ultimate OSS - 1 Year
 * Ultimate OSS - 1 Year w/ Support
 * Ultimate OSS - 3 Year w/ Support

### Who qualifies for the GitLab for Open Source program?

In order to qualify, projects must meet the program requirements listed on the [GitLab for Open Source application page](https://about.gitlab.com/solutions/open-source/join/). 

Here are some guidelines that help us make decisions about whether or not projects qualify:

**Examples of OSS projects that usually qualify:**
 * _Receives Donations_ -- The software is being developed by a community that gathers donations for covering costs. 
 * _Has an open governance model_ -- Projects with open governance models are likely to be accepted. For more information about open governance models, see this open source governance model article. The only model in there that would NOT qualify is the corporate governance model.

**Examples that usually do not qualify:**
 * _Open Core_ -- The software is being primarily developed by a company that uses the software for its core or base product. The company charges for use of the software at higher tiers. 
 * _Charging for Services_ -- The software itself is not being sold but the open source project is being primarily developed by a company that charges for services around that open source project. 

For more questions, please see our [GitLab for Open Source FAQ](/solutions/open-source/#FAQ) or contact us at `opensource@gitlab.com`

#### Federal Exception Policy
Unfortunately, we are not able to accept all open source projects that are affiliated with the US Federal government. Projects that are affiliated must work with a Sales representative to see if they qualify.

#### Strategic Qualification Exceptions
From time to time, we make strategic exceptions to our program requirements, such as to the [federal exception policy](/handbook/marketing/community-relations/opensource-program/#federal-exception-policy). These requests must be made by a GitLab Sales team member on behalf of a project. 

Requests for qualification exceptions can be made by filing a [new issue](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/general/-/issues/new) on the GitLab for Open Source program kanban board and selecting the `oss-qualification-request` template.

In order for this to be approved, both Account Executives and their Managers will need to sign off by checking the box next to their name in the submitted issue. If there is a relevant Technical Account Manager (TAM) associated with the account, they should be added to the issue in FYI so that they are notified of the exception request. 

## Application Process and Workflow
Qualifying open source projects can apply for self-hosted Ultimate or SaaS Gold by submitting the [GitLab for Open Source application form](/solutions/open-source/join/).

We follow then follow the **[community programs application workflow](/handbook/marketing/community-relations/community-operations/community-program-applications/)** to process applications.

For more information on what applicants can expect, please see the `Application Process` section of the [GitLab for Open Source application form](/solutions/open-source/join/).

## Resources and additional open source programs
There are a number of organizations that offer open source programs with discounts or free services just for open source projects. Here is a list of some of the programs we know that have been useful to pair with the GitLab for Open Source program:

 * [AWS](https://aws.amazon.com/blogs/opensource/aws-promotional-credits-open-source-projects/) - Provides promotional credits for hosting to qualifying open source projects.
 * [Discourse](https://free.discourse.group/) - Provides a zero dollar cloud hosted forum/mailing list replacement for open source projects.

### GitLab Forum & Docs
[GitLab Docs](https://docs.gitlab.com/) are a great resource for our program members since our docs are the single source of truth and answer a wide variety of questions program members may face.

If program members are unable to find what they're looking for in our docs, we ask that they post their question in the [GitLab Forum](https://forum.gitlab.com/). There, they can interact with other members of our community and many GitLab employees. We encourage program members to join the forum even if they don't have questions so that they can meet other members of our community.

### Submitting feedback: bugs, features, and more
In line with our Transparency value, GitLab has an [open roadmap](/direction/), so our community can always see what's ahead in the product priorities.

Feedback helps us to build the best product possible. Whether it's a bug report, a feature request, or feedback to our company, here's a guide for [how to submit feedback](/submit-feedback/).

**Make sure to Thumbs Up issues.**
Our team looks at the number of `Thumbs Up` votes as an indicator for priority, so make sure you're using that feature on issues as a quick way to give us feedback. Writing detailed comments of your use-case and thorough issue desciptions is another way you can help make it easier for our team to understand your needs and respond to your questions.

Just search for relevant issues on the [GitLab product project](/gitlab-org/gitlab), and start giving us your feedback!


## Updating the GitLab for Open Source members list

When the GitLab for Open Source program began, we kept track and publicly displayed the members who joined the GitLab for Open Source Program at https://about.gitlab.com/solutions/open-source/projects/. 

To do this, applicants submitted a Merge Request to the [GitLab OSS project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/gitlab-oss/). Whenever the applicant was approved, and as part of the process, the project was added to a [master list on the `/gitlab-com/marketing/community-relations/gitlab-oss` repository](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/blob/master/data/oss_projects.yml).

**Unfortunately, the [open source project list](/solutions/open-source/projects/) is now outdated because of the following reasons:**
 * The Merge Request step was removed from the application process to simplify the process (by lowering the barrier of entry for applicants), and in order to allow the program to scale.
 * The list became naturally outdated since some projects listed did not renew, so the list is not a reflection of active OSS projects on GitLab today.

This section talks about how we used to update the open source project list so that we preserve this knowledge as we plan for future iterations of an open source project directory.

While the original Merge Request process was ideally meant to keep the list updated, there was also a manual step involved in transferring the file from the `/gitlab-com/marketing/community-relations/gitlab-oss` repository to the GitLab's website repository.

These additional steps were required to update the GitLab for Open Source members list on the website:

1. Fetch the [latest list of GitLab for Open Source members on the `/gitlab-com/marketing/community-relations/gitlab-oss` repository](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/-/raw/master/data/oss_projects.yml)
1. Copy the contents of that file into the [GitLab website's `data/oss_projects.yml` file](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/blob/master/-/data/oss_projects.yml)
1. Commit the changes and submit an MR to merge the changes into the website's master branch
1. Once the MR has been merged, the GitLab for Open Source members list will be up to date at https://about.gitlab.com/solutions/open-source/projects

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59)
;"></i> At this time, the [master list on the `/gitlab-com/marketing/community-relations/gitlab-oss` repository](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/blob/master/data/oss_projects.yml) is only updated for applications, not for renewals. As such, it might contain data from projects who initially applied for the program but did not renew their yearly license.
{: .alert .alert-warning}

# <i class="far fa-newspaper" id="biz-tech-icons"></i> GitLab Open Source Partners
The GitLab Open Source Partners program seeks to build relationships with open source projects that have thousands of community members or users (or more). 

Through our partnership, we aim to gain insight to help us build a better product and navigate the challenges of being an open core, for-profit company. We also aim to create greater outreach through co-marketing and special initiatives.

Full program benefits and requirements can be found on the [GitLab Open Source Partners landing page](/solutions/open-source/partners/).

We have Open Source partners that use a variety of GitLab's products. In addition to Gold and Ultimate, some of our Open Source Partners use the [Community Edition](https://about.gitlab.com/install/ce-or-ee/) instead of the Enterprise Edition because of their strong FOSS values. 

## Email Templates
### Inviting project to apply
Hi [Name],

We'd like to invite you to join our [GitLab Open Source Partners](https://about.gitlab.com/solutions/open-source/partners/) program [1]. This program's aim is to feature your project in blogs, at GitLab events, and more, while also helping us engage with your community more regularly and learn more about your experience using GitLab. Since you're already part of our [GitLab for Open Source](https://about.gitlab.com/solutions/open-source/) program [2], and are a prominent open source project, we would love to have you as an open source partner. Just as an FYI, there are no membership dues for this program as it is completely free of cost.

Please let me know if you're interested in connecting to learn more about this partnership opportunity. Feel free to grab time on my calendar for a short call: [calendly link], or if you prefer to chat more via email, we can do that too.

Best,
[Name]

 * [1] https://about.gitlab.com/solutions/open-source/partners/
 * [2] https://about.gitlab.com/solutions/open-source/

### Public Migration Tracking Issue
We use public migration tracker issues to help our OSS Partners through and beyond migration.

Public migration tracking issues help us understand which issues are blockers, urgent, important, and nice-to-have for our OSS Partners. It also has the added benefit of helping them get used to our workflow so that they can start giving us product feedback and helping us improve the product together.

Follow these steps to create a public migration tracking issue:

 1. Create a [New Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new) in the `gitlab.com/gitlab-org/gitlab` [project](https://gitlab.com/gitlab-org/gitlab/)
 1. Select the `Migrations` template in the description section where it prompts you to select a template, and click on `Apply template`
 1. Add in as much information as possible. Once finished, submit it.

 Examples of public issue trackers:
  * [KDE](https://gitlab.com/gitlab-org/gitlab/-/issues/24900)
  * [Eclipse Foundation](https://gitlab.com/gitlab-org/gitlab/-/issues/195865)
  * [Freedesktop](https://gitlab.com/gitlab-org/gitlab/-/issues/217107)

## Editorial plan for OSS Partners
The Open Source Program Manager worked with the Content team on an editorial strategy to meet the needs of the Open Source Partners program. Posts about technical content and open source partner migrations tend to be very popular, so the Content team collaborates by brainstorming topics and doing reviews. 

The plan is to publish posts on the [GitLab Blog](/blog/) that are related to the Open Source Partner program and to feature these on the [Open Source Partners page](/solutions/open-source-partners). 

Marketing opportunities: 
 * Migration announcement
 * Migration phase complete
 * Case Study
 * Technical challenge and resolution, use case that aligns with other marketing campaigns such as CI/CD, security, or project management

Resources: 
 * [Template - OSS Partner Use Case Blog Posts](https://docs.google.com/document/d/1oRwrwoo6PkuqafAsde11mqCnMvAI2KNZ9E7AfnGg9Fw/edit#) - includes template blog post questions to ask OSS Partners in order to publish posts about migrations, CI/CD, Security, and Project Management use cases.
 * [OSS Blog Posts and Views](https://docs.google.com/spreadsheets/d/1LpgSudtcgXDkPb7XX-4s2PW6vYAQZpDgQRdGw7OWqBY/edit#gid=0)

## How to add a new logo to the Open Source Partners page

Our Open Source Partners are shown on the [Open Source Partners](/solutions/open-source/partners) page and the main [GitLab for Open Source program](/solutions/open-source/) page. These are the steps to add a new logo to those sections.

### 1. Add a new org to the yml file
You'll need to add an entry to the `data/customers.yml` file using GitLab's WebIDE.

Follow this format (you can copy and paste it at the bottom of the customers.yml file):

```markdown
- name: ORG NAME
  logo: /images/organizations/logo_ORG NAME.svg
  logo_color: /images/organizations/logo_ORG NAME_color.svg  
  industry_type: 'open source software'
  home_url:  
  landing: false
  opensource_partner: true
```

Example:

```markdown
- name: Inkscape
  logo: /images/organizations/logo_inkscape.svg
  logo_color: /images/organizations/logo_inkscape_color.svg  
  industry_type: 'open source software'
  home_url: https://inkscape.org
  landing: false
  opensource_partner: true
```

### 2. Prep the logos
#### Create two SVGs: one color, one grey
 1. **Get an SVG.** Make sure you have an SVG file. If you weren't provided with one, look for a vector (`*.svg`) version of the logo in the official channels (e.g. the [ARM trademarks page](https://www.arm.com/company/policies/trademarks/guidelines-corporate-logo) has SVG as one of the download options). If you can't find the SVG file on their channels, you may need to Google it and find one on Wikimedia Commons. Download the original file.
 1. **Duplicate SVG and name each copy.** Duplicate the file and name each of the two files as follows:
     * `logo_ORG Name.svg` -- Example: `logo_inkscape.svg`. This will be the greyscale image that will be shown on the customer reference page.
     * `logo_ORG Name_color.svg` -- Example: `logo_inkscape_color.svg`. This will be the color image that displays on the Open Source Partners page.
 1. **Create greyscale logo.** These instructions are specifically for using [Canva](https://www.canva.com/) as the image editor. You can also use Inkscape, or a native Mac image editor app.
     * Upload the SVG image to Canva. Create a new "logo" that is square and add the logo to it. Resize the logo so that it takes up most of the space on the background.
     * Change the color of the logo to Hex Color Code: Dark Grey `#444444`
     * Export the greyscale logo as an SVG with a transparent background make sure that it is called: `logo_ORG Name.svg` as detailed above.

#### Optimize the SVGs
Do this for both color and grey logos:
 1. Go to this SVG optimization website: https://jakearchibald.github.io/svgomg/ and upload one of the logos
 1. Make sure that the `Prefer viewBox to width/height` factor is toggled `On`. It should be near the bottom of the list of optimization factors.
 1. Export the SVG and save

### 3. Upload the logo via WebIDE
 * Go to `source/images/organizations` and upload the two prepped logos

Once you've completed these steps, check to make sure that the pipleline works, and check out the preview. If it looks ok, have someone review and merge!

### Review the changes
Unfortunately, the review app will not automatically show you the preview. Here's how to generate a link to preview the changes.

#### Why does the View App not point to the correct webpage?

The View App lets you preview your changes. It detects which files have been changed, and then creates a URL, or a set of URLs, pointing to the built pages with the changes.

Since this type of merge request (adding a new logo to the OSS partners page), does not change any HTML files, the View App does not know where to point to. It defaults to the GitLab home page.

#### How do I get the review app to show the correct page?

Since the `View App` points to the GitLab home page for this build as mentioned above, we need to add the path to the webpage you'd like to preview.

To do this, you need to be familiar with four parts of the View App URL:
1. **The beginning of the URL:** `https://` -- it doesn't include "www"
1. **Your branch name:** You can copy this from your Merge Request (MR) in the "Request to Merge" section directly under the MR description.
1. **The View App snippet:** `.about.gitlab-review.app` -- this is what builds the preview
1. **The path to the webpage you'd like to preview:** This is everything after "about.gitlab.com/" on the webpage you're editing

Example:
1. `https://`
1. `nuritzi-master-patch-24835` (branch name)
1. `.about.gitlab-review.app`
1. `/solutions/open-source/partners` (path to the webpage I want to preview)

This formula creates: `https://nuritzi-master-patch-24835.about.gitlab-review.app/solutions/open-source/partners`. This link allows you to preview the MR changes for the `nuritzi-master-patch-24835` branch on the GitLab Open Source Partners page.

For ease of use, here's the formula:
```
`https://` [your branch] `.about.gitlab-review.app` [rest of the path to the page you want to build]
```

You should now be able to create preview links when the View App doesn't work!


# <i class="far fa-newspaper" id="biz-tech-icons"></i> Consortium Memberships and Sponsorships

## What are Consortiums?

We define Consortiums as (often open source) groups that have come together to further a technology cause. The prototypical Consortium would be the [Linux Foundation (LF)](https://en.wikipedia.org/wiki/Linux_Foundation), which is a non-profit technology consortium founded in 2000 as a merger between Open Source Development Labs and the Free Standards Group. The Linux Foundation's mission is to standardize Linux, support its growth, and promote its commercial adoption. It hosts and promotes the collaborative development of open source software projects.

### Why is Consortium marketing important?
Consortiums are marketing giants in the enterprise technology ecosystem. The success of the Cloud Native Computing Foundation today, the OpenStack foundation in the past, and many others, prove the value of technology thought leadership, branding, and engineering alignment. While these memberships sit within the budget of the Community Team's Open Source program, the [Developer Evangelism team](/handbook/marketing/community-relations/developer-evangelism/) focuses on consortium marketing and integrating into the community to bring the GitLab technical perspective to the right conversations.

### Membership evaluation Criteria
Here are some of the factors we keep in mind when considering to join a new consortium:   

| Goals | Indicators | Examples of Indicators |
| ------ | ------ | ------ |
| Awareness Opportunities | Size of organization / contributor / member base: how many people are part of the organization’s community? | Monthly authenticated vs. non authenticated users visiting the sit. Annual users who perform a contribution activity of any kind. Annual users who perform a code-related contribution activity |
|  | Frequency and impact of marketing opportunities: what kind of communication channels do they have? Will we appear in official channels? How prominent is our placement?  | Channels may include: Social, Newsletters, Blogs, Events (Size of each event) |
| Ease of Collaboration | Dedicated marketing resources / point person: Does the organization have marketing capacity? | How mature is the organization's brand and marketing portions? Is marketing handled by volunteers, paid employees? |
|  | Relationship: how responsive is the person in charge of the relationship? | An alternative metric might be: Time-to-Execute for a few standard communication types. For example: from case study ideation to execution - 1 week? 1 month? 1 quarter? |
|  |  | Understanding of community gates: Can the foundation approve directly - is there a community feedback process, is board approval required, etc |
| Contribution and hiring pool opportunities | Active community: How active is the community and do they know their own community’s health, engagement, etc? | Frequency of contribution? Rate of adoption? |
|  | Hiring opportunities: Are there opportunities to recruit from the community's talent pool?  | What is the growth of the community or foundation itself? Are there job opportunities within that software ecosystem (are people hiring contributors from this community in general)? Are there job boards, or other professional development activities? |
|  | What kind of informal and formal ways are there for us to contribute, and do they align with our interests? | Working groups? Advisory boards? Special initiatives? |
|  | Can GitLab participate in the project's roadmap in ways that creates mutual value?  | Example: Promoting adoption of GitLabCI to improve project testing and also expose contributors to GitLab's tools for their day-jobs/for-profit ventures.  |

### Current memberships
We are currently members of the following consortiums:

| Consortium | Level | Activities | Membership Details (internal only) |
| ------ | ------ | ------ | ------ | 
| [Linux Foundation](https://www.linuxfoundation.org/) | [Silver](https://www.linuxfoundation.org/membership/members/) | Eric Johnson is currently on the Board | [Membership Details](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships/-/issues/5) |
| [Cloud Native Computing Foundation (CNCF)](https://www.cncf.io/) | [Silver](https://www.cncf.io/about/members/) | Brendan O'Leary currently on the board | [Membership Details](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships/-/issues/1) |
| [Fintech Open Source Foundation (FINOS)](https://www.finos.org/) | [Silver](https://www.finos.org/members) | Collaborating on Legend project | [Membership Details](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships/-/issues/7) |

*Full details of our memberships with these organizations is available to GitLab team members only: [Community Relations Consortium Memberships](https://docs.google.com/spreadsheets/d/1h7DE5rDCj28ccYzJAZAnVwzEA1D0pxmrJoIqFLSlfL0/edit#gid=0).*

### Elections for Board of Directors Opportunities
Some of the consortium memberhsip in which we participate allow members to run for their Board of Directors. Here is more information about how these elections run and how GitLab has run the internal nomination and campaigning process. 

#### Internal Nominations
An issue was created for each nomination so that GitLab team members who were interested in running could discuss, as well as for internal nominations to occur. Teams involved: Community Relations, Alliances, Marketing leadership (CMO, Director of Corporate Marketing). 

#### Campaigning
Once GitLab candidates were nominated, the Community Relations team helped run campaigns for the candidates. A spreadsheet was created with member companies, and a campaign message template was created. GitLab team members were made aware of the election and were given access to the spreadsheet and template in case they wanted to help campaign (announced on the `#whats-happening-at-gitlab` Slack channel). 

#### Promoting 
The social media team is able to promote elections notification news. They need a page to point people to, preferably an updated page that lists the board of directors, or else a social media post from the organization that mentions the election results. 

#### Membership Specific Details
##### Linux Foundation
There are two Board Seats for Silver members and they are open for 2 year terms. This means that each year, only one seat is open for Silver member nominations. 

The election process was not widely publicized for the 2021 election cycle and only the Primary/Voting contact was notified. Nominations had to be submitted via a form. 

The entire election process lasted about two weeks: Jan 25th - Feb 11th. For more information, see this issue about the [2021 Linux Foundation Nominations](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships/-/issues/12) (available to view by GitLab team members only).

##### CNCF
Board of Directors - This election ran from December 9, 2020 to January 11, 2021 for the 2021 seat. Only one board seat was available and candidates sent self-nominations via a form. 

Marketing Chair - This election ran from December 2nd - December 17th in 2020 for 2021 seats. The Marketing contact was sent an email with voting instructions and candidates sent their self-nomination emails to the CNCF marketing list. 

### Other sponsorship types
We have a small budget to sponsor events that allow us to engage with and build relationships among our current open source partners' communities. All other event sponsorship requests, are handled by our [field marketing team](/handbook/marketing/revenue-marketing/field-marketing/#3rd-party-events).  
