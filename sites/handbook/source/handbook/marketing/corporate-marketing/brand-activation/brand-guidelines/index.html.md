---
layout: handbook-page-toc
title: "Brand Guidelines"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand Guidelines
{:.no_toc}

### Brand Standards
Last updated: 2021-02-01
<html>
  <head>
    <title>PDF Viewer</title>
  </head>
  <body>
    <div>
      <object
        data='/resources/gitlab-brand/gitlab-brand-standards.pdf'
        type="application/pdf"
        width="820"
        height="650"
      >

        <iframe
          src='/resources/gitlab-brand/gitlab-brand-standards.pdf'
          width="820"
          height="650"
        >
        <p>This browser does not support PDF!</p>
        </iframe>

      </object>
    </div>

  </body>
</html>
