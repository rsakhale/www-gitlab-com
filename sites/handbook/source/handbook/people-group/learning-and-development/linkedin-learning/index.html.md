---
layout: handbook-page-toc
title: "LinkedIn Learning"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

In January 2021, GitLab implemented LinkedIn Learning to provide our team members with more off the shelf learning resources. With a growing company, the needs for learning are growing as well and we are excited to provide this opportunity to our team members.  

## Benefit of LinkedIn Learning

Utilizing LinkedIn Learning will be beneficial to your professional development. With access to LinkedIn Learning, you will be able to access over 8,000 courses on many different topics. 

### How to incorporate LinkedIn Learning into your day-to-day role 

We encourage all team members to [take time out to learn](/handbook/people-group/learning-and-development/learning-initiatives/#take-time-out-to-learn-campaign). If you are working on a project and realize you need to know more about a certain topic or a certain tool, you can go to GitLab Learn and search for LinkedIn Learning couses to see if there are resources avialble to you. 

### Take Time out to Learn

You can take time to learn about something that will help you in your current role. You can learn about things that can help you in a future role (maybe you are an aspiring manager). You can learn more to improve current skills or learn entirely new skills. You can also take time to learn about things that are fun for you like graphic design, drawing, or mindfulness. 

Finding courses and skills to learn just got easier with access to LinkedIn Learning. We have a [take time out to learn campaign](/handbook/people-group/learning-and-development/learning-initiatives/#take-time-out-to-learn-campaign) where we encourage our team members to take time out of their work week to learn. Focus Fridays create designated meeting-free space within our weeks and are a great opportunity to take time out to learn. 

## Recommended Learning Paths

We have outlined recommended LinkedIn Learning paths for different roles at GitLab. These links will take you to GitLab Learn and if you decide to take the course it will open a new tab for the course in LinkedIn Learning. This is not an exhaustive list, but it is a good start if you are new to using the tool. 

### All Team Members 
- [Confronting Bias: Thriving Across our Differences](https://gitlab.edcast.com/insights/ECL-393c827e-4175-46e2-bdfb-768c7cb49f27)
- [Diversity, Inclusion, and Belonging](https://gitlab.edcast.com/insights/ECL-6de0468c-8f07-4cb8-8cca-4ab2de0dfd07)
- [Supporting your mental health while working from home](https://gitlab.edcast.com/insights/ECL-41f42b4f-ad92-4591-8e3a-1c6a780074fc)
- [Psychological Safety: Clear Blocks to Innovation, Collaboration, and Risk-Taking](https://gitlab.edcast.com/insights/ECL-2c1083e4-2e40-4778-9d3c-4bc86145420b)

### Aspiring People Leaders 
- [Leading Yourself](https://gitlab.edcast.com/insights/ECL-4bb8cdde-b9be-4c99-99a7-129956cd712d)
- [Make the move from individual contributor to manager](https://gitlab.edcast.com/insights/ECL-baffb269-3b6e-4288-a134-dfb7f3ff4fcc) 
- [Leading without Formal Authority](https://gitlab.edcast.com/insights/ECL-ae279fc1-1bb6-48a0-8c02-a549ec6f2b30)
- [Being a good mentor](https://gitlab.edcast.com/insights/ECL-aa4516de-3675-4eb5-8751-49b1d8f3bfa9)
- [Developing credibility as a leader](https://gitlab.edcast.com/insights/ECL-c1f3ddb3-fa12-41a2-9a04-fce688965b8f)

### People Leaders
- [Building High Performance Teams](https://gitlab.edcast.com/insights/ECL-735601ad-b5ea-4ae3-894e-d49cd47540b8)
- [Developing your Emotional Intelligence](https://gitlab.edcast.com/insights/ECL-b5c78da2-0dfb-4a90-aedd-c6cf825ca46e)
- [Using feedback to drive performance](https://gitlab.edcast.com/insights/ECL-84ae3443-83e0-417e-916a-19c43606941d)
- [Delegating Tasks](https://gitlab.edcast.com/insights/ECL-fbda05e5-ad98-401e-a6a9-6dd0c9c678f5)
- [Coaching and Developing Employees](https://gitlab.edcast.com/insights/ECL-98941f0d-58dc-46ab-bbe6-90fbef4270bf)
- [Change Management Foundations](https://gitlab.edcast.com/insights/ECL-263cc353-0596-4173-8471-e8ed52251864)
- [Inclusive Leadership](https://gitlab.edcast.com/insights/ECL-c2ba0aa4-11fb-4a06-bae0-ad480265fc31)

### Senior Leaders and Executives 
- [Executive Leadership](https://gitlab.edcast.com/insights/ECL-59d436b8-bcc4-4d16-bfce-a34de7d34853)
- [Crisis Communication](https://gitlab.edcast.com/insights/ECL-25407918-174a-4fc6-8a0e-55ad239ce582)
- [Developing Executive Presence](https://gitlab.edcast.com/insights/ECL-1766101d-3f71-41c9-9632-8062e3e42492)
- [Executive Decision Making](https://gitlab.edcast.com/insights/ECL-02cb7a4e-f8ec-4150-9d22-fa80e8b12cdb)
- [Risk Taking for Leaders](https://gitlab.edcast.com/insights/ECL-96775828-7b6e-4e0c-a9dc-5fd8c31457ee)
- [Leading Globally](https://gitlab.edcast.com/insights/ECL-2ed336c8-ef0d-42be-8c28-c8ce7ab58dbb)
- [Succession Planning](https://gitlab.edcast.com/insights/ECL-952712ae-c141-41ce-bc45-74fa2016c8bd)
- [Diversity: The best resources for achieving business goals](https://gitlab.edcast.com/insights/ECL-9ef8bd9d-f8b6-40d0-ae95-e0e8e315c97f)
- [Managing experienced managers](https://gitlab.edcast.com/insights/ECL-ac19941f-f53b-4c27-8193-48ff7ceb8a41)

### Professional Development 

All are outlined under [Continuing Education (CEU)](https://www.linkedin.com/learning/topics/continuing-education-ceu?u=2255073) on LinkedIn Learning. 

- [Society for Human Resource Management (SHRM) professional development credits (PDCs)](https://www.linkedin.com/learning/topics/society-for-human-resource-management-shrm?u=2255073) 
- [Project Management Institute (PMI)](https://www.linkedin.com/learning/topics/project-management-institute-pmi?u=2255073)
- [HR Certification Institute (HRCI)](https://www.linkedin.com/learning/topics/hr-certification-institute-hrci?u=2255073) 
- [Computing Technology Industry Association (CompTIA)](https://www.linkedin.com/learning/topics/computing-technology-industry-association-comptia?u=2255073) 
- [International Institute of Business Analysis (IIBA)](https://www.linkedin.com/learning/topics/international-institute-of-business-analysis-iiba?u=2255073) 
- [NASBA Continuing Professional Education (CPE)](https://www.linkedin.com/learning/topics/nasba-continuing-professional-education-cpe?u=2255073)

## How to access LinkedIn Learning

All GitLab Team Members will be sent an invite to LinkedIn Learning. If you did not recieve an invite, please follow this process for requesting access to a license for paid LinkedIn Learning content: 

1. Ensure your GitLab email address is on your LinkedIn Profile  
1. Reach out to the Learning & Development team in the `#learninganddevelopment` Slack Channel
1. You will receive an email notification from LinkedIn Learning once you have been added to the system. 
1. To access LinkedIn Learning content, go to our learning platform, [GitLab Learn](https://gitlab.edcast.com/) and search for skills or topics you want to learn more about. When you select a LinkedIn Learning course it will open a new tab with the course directly on LinkedIn Learning. 

## Searching for LinkedIn Learning Courses

1. Open [GitLab Learn](https://gitlab.edcast.com/)
1. Click the search bar at the top and type in what you are looking for
1. If you want to filter down to LinkedIn Learning courses specifically, you can click “All Filters” in the top right. Then check the “LinkedIn Learning” box and click “Apply” (this will pull in only Linkedin Learning courses on the topic you are looking for) 
1. Once you have found the course you are interested in, select that smartcard 
1. On the smartcard click "view more" and this will open a separate tab with the LinkedIn Learning course 
1. Click "play" to begin the course 

<iframe width="560" height="315" src="https://www.youtube.com/embed/luoLfUJHhrc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Sharing Your LinkedIn Learning Certificate

Once you have completed a course on LinkedIn Learning, you will recieve a certificate. You will have the option to download and/or share it on your LinkedIn Profile. The video below walks thorugh how to do both. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/GpWhSB-qrF0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## LinkedIn Learning Cohorts

Self-organizing a cohort to complete a LinkedIn Learning course together can be a great way to keep each other accountable, discuss your learning, and connect with new team members. Similar to our [book clubs](/handbook/leadership/book-clubs/), we encourage team members to organize LinkedIn Learning cohorts to learn together.

Use this checklist when organizing your LinkedIn Learning cohort. Here is an example of how the [GitLab Women TMRG organized and implemented a LinkedIn Learning cohort](https://gitlab.com/gitlab-com/women-tmrg/-/issues/10).

1. Organize a cohort of learners. Consider reaching out to your team, the #what's-happening-at-GitLab channel, or a TMRG you're part of. Pose the opportunity to learn together and see what response you get!
1. Explore courses on LinkedIn Learning. We have suggestions on this page for all team members, but feel free to get specific in an area you're passionate about!
1. Vote or decide on a cousre to take together. 
1. Set a completion goal. Be realistic with how much time you have to learn each week. Consider a 2 or 3 month completion goal.
1. Decide how you want to collaborate. You may want to meet bi-weekly to discuss synchronously or use issues to collabrate async.
1. Build your learning pathway in GitLab Learn. Curate existing LinkedIn Learning SmartCards into the pathway you'd like to create. Consider adding a badge to the pathway as well. Documentation for building pathways can be found in our [GitLab Learn admin docs](/handbook/people-group/learning-and-development/gitlab-learn/admin/)
1. Choose a facilitator. This facilitator is expected to organize discussion issues, set due dates, and send preriodic reminders to the cohort to keep members accountable
1. Create a temporary Slack channel. This space can be used as a place to remind members about upcoming deadlines, plan synchronous calls, and ask questions. 
1. Have a celebration! When you've completed the course, consider having a celebration to recap your learning, share takeaways, and recognize your accomplishments. Share your takeaways and badges on your LinkedIn profile if you'd like!

## Learning and Development Team

The L&D Team will provision and deprovision access to LinkedIn Learning. If you are on the L&D Team and would like to learn more about LinkedIn Learning, we recommend the [Getting Started as a LinkedIn Learning Admin](https://gitlab.edcast.com/insights/ECL-743da12d-7ead-4d9f-8deb-62a6c93c0790) course. 

### Provisioning Access 

The L&D Team will provision access to team members who want access to paid content on LinkedIn Learning by going through the following steps: 

1. Log in to LinkedIn Learning 
1. Click "Go to Admin" in the top right 
1. Hover over the "+" in the top right and click "Add Learners" 
1. Click "Add new users" and then "Add new users by email" 
1. Enter team members GitLab email address 
1. Click "Confirm" in the bottom right 

### Deprovisioning Access 

The L&D Team will deprovision access to LinkedIn Learning to team members who leave GitLab by going through the following steps: 

1. Log in to LinkedIn Learning 
1. Click "Go to Admin" in the top right 
1. Use the search bar to search by name or email 
1. Click the three dots at the right end of the row with their name
1. Click "Manage licenses" 
1. Under "Assign a license" change the selection to "No license" 
1. Click "Confirm"

If you are on the L&D Team, you can watch a [video walkthrough](https://drive.google.com/file/d/1x0jhZ09xLN2VHMD_pc-ZUQu3FmXivg7e/view?usp=sharing) of the deprovisioning process. 

## User access process with the EdCast

All GitLab Learn users will be able to access free content on LinkedIn Learning. 

If you have activated a LinkedIn Learning license, you will also be able to access the paid content on LinkedIn Learning. You can find LinkedIn Learning courses by going to GitLab Learn and utilizing the search bar at the top to search for a topic or skill. 

## Coordination with EdCast

### Set Up 

Our integration and configuration was set up in conjunction with our partners from both EdCast and LinkedIn Learning using the [Integration Guide](https://docs.microsoft.com/en-us/linkedin/learning/lms-integrations/integration-docs/edcast) and the [Configuration Guide](https://business.linkedin.com/content/dam/me/learning/en-us/pdfs/LinkedIn-Learning-EdCast-configuration-guide.pdf). 

## LinkedIn Learning KPIs 

- Engagement survey scores for L&D
- Number of courses taken
- % of licenses used
- Average number of courses each person takes

## FAQs 

- I didn't recieve an invite for LinkedIn Learning. What should I do? 
   - Please reach out to the Learning and Development team in slack in the `#learninganddevelopment` channel. 
- I recieved an invite for LinkedIn Learning, but can no longer find it. What should I do? 
   - You can search your inbox for the subject `subject line`. If you still can't find the email, please reach out to the Learning and Development team in slack in the `#learninganddevelopment` channel. 
- I can't find LinkedIn Learning courses in GitLab Learn. What should I do? 
   - If you cannot access LinkedIn Learning content in GitLab Learn, it is likely because you have not activated a LinkedIn Learning license. First, comfirm that you have activated a LinkedIn Learning license. You can do that by going to [LinkedIn Learning]() to see if you have an account. If you do not have a license or an invite, please reach out to the Learning and Development team in slack in the `#learninganddevelopment` channel. 
- How do I access content in LinkedIn Learning? 
   - To access LinkedIn Learning content we recommend that you go to our GitLab Learn platform and search for skills and topics you want to learn more about that way. You can filter content to only show LinkedIn Learning content. 
