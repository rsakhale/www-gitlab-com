---
layout: handbook-page-toc
title: "Spending Company Money"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
In keeping with our values of results, freedom, efficiency, frugality, and boring solutions, we expect GitLab team members to take responsibility to determine what they need to purchase or expense in order to do their jobs effectively. We don't want you to have to wait with getting the items that you need to get your job done. You most likely know better than anyone else what the items are that you need to be successful in your job.

The guidelines below describe what people in our team commonly expense. Some of the items below have the average amount that folks typically spend on them as a helpful guide. If you need to spend more to get the right tool for you to work productively, please do so. If you are uncertain about how much reasonable to spend on a more expensive item, please reach out to your manager and ask for help.

**(Note: All employees and contractors must protect our company assets, such as equipment, inventory, supplies, cash, and information. Treat company assets with the same care you would if they were your own. No employee or contractor may commit theft, fraud or embezzlement, or misuse company property.)**

## Guidelines

1. Spend company money like it is your **own** money. _No, really_.
1. You don't have to [ask permission](https://m.signalvnoise.com/if-you-ask-for-my-permission-you-wont-have-my-permission-9d8bb4f9c940) before making purchases **in the interest of the company**. When in doubt, do **inform** your manager prior to the purchase, or as soon as possible after the purchase.
1. It is generally easiest and fastest for you to make any purchases for office supplies yourself and expense them. If you are unable to pay for any supplies yourself, please follow the [Advance instructions](/handbook/spending-company-money/#advance).
1. You may privately use GitLab property, a MacBook for example, to check your private e-mails or watch a movie as long as it does not violate the law, harm GitLab, or interfere with [Intellectual Property](/handbook/general-guidelines/#sts=Intellectual Property). More details can be found in the [internal Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/).
1. If you make a purchase that will cost GitLab $1000 USD per item (or over), this is classed as company property, you will be required to return the item(s) if you leave the company.
1. Employees: file your expense report in the same month that you made the purchase. Contractors: include receipts with your invoices.
1. Any non-company expenses should not be paid with a company credit card.
1. If team members submit expenses for reimbursement for personal purchases that are not covered in the expense policy, this is a violation of our [Code of Business Conduct & Ethics](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/).  


## Expense Policy

1. Max Expense Amount - [$5,000 USD](/handbook/total-rewards/compensation/#exchange-rates) - NOTE - If you are a corporate credit card holder, please refer to the [corporate credit card policy section](/handbook/finance/accounting/#credit-card-use-policy) for those specific instructions and thresholds.
1. Receipts are required for all expenses.
1. Expenses must be submitted within 30 days of purchase/spend. This helps the Company to better manage our budget and ensures accurate monthly financial reporting.

For further information on Expenses, visit the [Expenses handbook section](/handbook/finance/expenses/). 

The aforementioned section covers details including:
1. Reimbursing coworking fees and external office space
1. Office equipment and workspace supplies
1. Work-related online courses and professional development certifications
1. Year-end holiday party budget 
1. Travel expense guidelines 

## Advance

These instructions apply if a team member is unable to purchase items, for whatever reason.

1. New team member will make a list of requested items and prices, noting if they are out of the budget range listed on this page (if applicable), and send to their manager for approval. We ask that only one list be sent, versus multiple lists.
1. The team member's manager will send the approved (or edited) list to Accounting (nonuspayroll@gitlab.com OR uspayroll@gitlab.com, and CC ap@gitlab.com) for final approval and dispensation.
1. Once approved, Payroll will send the team member an invoice template to fill with the approved items, prices and the team member's bank information.
1. The approved final amount will be sent to the team member's bank and they can then purchase their approved items.

## Approving Expense Reports

1. Expensify will send a notification email when a team member submitted an expense report
    * Click on the report name in the body of the email
    * Review each expense for the correct amount of the receipt and the report
    * Check for customers or project name if applicable under Tag
    * We require a receipt for all expenses
    * Select [Approve and Forward] option and Expensify pre-populated the email address.  Note, Expensify is updating their coding to address a small glitch in this field.  If it is empty, please send it to **Montpac** (gitlab-expensify-mp@montpac.com)
    * **Important** - please do not use [Final Approval] because Expensify will not send the email notification for payment approval and it will delay the reimbursement process
    * Manager can delegate the approval process during PTO:
        *  Settings
        *  Your Account
        *  Vacation Delegate
        *  Enter the email address of the backup approval
    * All expense question(s) can be addressed via expenses@gitlab.com or in the #Finance and #expense-reporting-inquires
Slack channel

1.  **Expenses Reports approval deadline**
    * Australia, Germany, New Zealand, Netherlands, United States - After approval completion by manager and accounts payable
    * United Kingdom - All expense reports must be approved by the Manager and Accounts Payable no later than the 14th of each month.  Team members - please be sure to submit your report(s) couple days before the due date so your manager and accounts payable have enough time for approval.
    * Canada - All expense reports must be approved by manager and accounts payable before 1st day of each payroll period.
    * All non-US contractors - All expense reports must be approved by manager and accounts payable no later than the 8th of each month. Team members - please be sure to submit your report(s) couple days before the due date.



## Mileage

Mileage is reimbursed according to local law: [US rate per mile](http://www.irs.gov/Tax-Professionals/Standard-Mileage-Rates), [rate per km in the Netherlands](http://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/zakelijk/auto_en_vervoer/auto_van_de_onderneming/autokosten/u_rijdt_in_uw_eign_auto), or [rate in Belgium](https://fedweb/belgium.be/nl/verloning_en_voordelen/vergoedingen/vergoeding-voor-reiskosten) . Add a screenshot of a map to the expense in Expensify indicating the mileage. 

## Internet/Mobile Subscription

 * For team members outside the Netherlands: follow normal expense report process. 
 * For team members in the Netherlands : fill in and sign the [Regeling Internet Thuis](https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq) form and send it to the People Experience team at people-exp@gitlab.com . The People Experience team will then send it to the payroll provider in the Netherlands via email. The details of the payroll provider can be found in the PeopleOps vault in 1Password under "Payroll Contacts". 
 1. VPN service subscription. Please read [Why We Don't Have a Corporate VPN](/handbook/security/#why-we-dont-have-a-corporate-vpn) for more information, and check out our [Personal VPN](/handbook/tools-and-tips/personal-vpn/) page regarding usage at GitLab.
 1. Mobile subscription, we commonly pay for that if you are a salesperson or executive, or if your position requires participation in an on call rotation. If your device cost is part of your monthly plan please do not include the device on the expense reimbursement. 
 1. Telephone land line (uncommon, except for positions that require a lot of phone calls)
 1. Skype/Google Hangouts calling credit (uncommon, since we mostly use Zoom)


## Laptops

1. The [IT Ops](/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptops) page outlines laptop purchasing for new hires and for repairs and EOL for existing employees.
1. Laptops paid for by the company are property of GitLab and need to be reported with serial numbers, make, model, screen size and processor to IT Ops by adding it to this form: [GitLab laptop information](https://docs.google.com/forms/d/e/1FAIpQLSeUOlP11qeLdLZHTI62CFr7MSHAoI_1M6CRpnUA6fegkEKCOQ/viewform) for proper [asset tracking](/handbook/finance/accounting/#fixed-asset-register-and-asset-tracking). Since these items are company property, you do not need to buy insurance or an extended warranty for them unless it is company policy to do so (for example, at the moment we do not purchase Apple Care or extended warranties). You do need to report any loss following [Lost or Stolen Procedures](/handbook/people-group/acceptable-use-policy/#lost-or-stolen-procedures) or damage to IT Ops as soon as it occurs.
1.  **Repairs to company issued equipment.**
    * If you need to replace a battery or something small that does not affect the productivity or speed of the device, please go ahead and get that small item replaced and expensed.
    * Please get approval from your Manager if your equipment appears to be damaged, defective, or in need of repair. Business Operations can advise on next steps to ensure you have the proper equipment to work.
    * For loaner laptops: Do not hesitate when expensing a loaner laptop while your primary laptop is being repaired. Use your best judgment identifying a local vendor. Please check out our [laptop repair](https://about.gitlab.com/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptop-repair) page for more info.

## English lessons

At GitLab the lingua franca is [US English](/handbook/communication/#american-english), when English is not your native language it can limit you in expressing yourself.

## Currency Conversion
If you need help with calculating the conversion rate to $ USD, [please refer here.](https://www1.oanda.com/currency/converter/)
