---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@sabrams](https://gitlab.com/sabrams) | 1 | 300 |
| [@leipert](https://gitlab.com/leipert) | 2 | 200 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@reprazent](https://gitlab.com/reprazent) | 1 | 200 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@sabrams](https://gitlab.com/sabrams) | 1 | 300 |
| [@leipert](https://gitlab.com/leipert) | 2 | 200 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@reprazent](https://gitlab.com/reprazent) | 1 | 200 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


