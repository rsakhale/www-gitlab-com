---
layout: handbook-page-toc
title: Expenses
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Welcome to the Expenses page!  You should be able to find answers to most of your questions here.  If you can't find what you are looking for, then:
- **Chat channel**: `#expense-reporting-inquires`
- **Email**: `ap@gitlab.com`

GitLab utitilizes Expensify as our team member expense reimbursement tool. All team members will have access to Expensify within 2 days from their hire date.  If you didn't receive an email from Expensify for your access, please contact ap@gitlab.com. Note: Ensure to opt-out from news by logging in and navigating to `Settings > Preferences > Contact Preferences` and un-checking the box at `Relevant feature updates and Expensify news`.

Expense reports are to be submitted at least once a month. Additional information on getting started with Expensify and creating/submitting expense reports can be found [here](https://community.expensify.com/discussion/5922/deep-dive-day-1-with-expensify-for-submitters/p1?new=1).

### Redacting Personal Information from Receipts

As the service does *not* require your personal information in order to process the reimbursement, **please redact your personal information before uploading receipts** to protect your privacy. Managers should verify receipt images do not contain personal information prior to approving the expense request. Any receipts containing personal information should be rejected and sent back to be corrected. Personal information includes:
* Your shipping and billing address(es)
* Your credit card information
* Your full name
* Any other data that is personal to you that you would not want publicly known

At a minimum, information about the items/products purchased and the total purchase amount should be visible so managers can validate the expense reimbursement amount requested along with the actual items and total purchase price.

<details>
<summary>Instructions for redacting information</summary>
The best way to capture only the necessary receipt information is to take a screenshot of your receipt that includes only the items purchased and total amount. On your Mac, you can press the <b>Command + Shift + 4</b> keys, which will turn the mouse pointer into a crosshair. Click and drag with the crosshair to take a screenshot. The screenshot is automatically saved to your desktop and can be uploaded to your expense report.
<br>
Alternatively, the Preview application built in to macOS offers a redaction tool that is hidden away in the Markup toolbar when viewing a PDF. When used, this will permanently delete any content marked for redaction when the PDF is saved allowing for it to then be securely uploaded to your expense report.
<br>
If you are updating a rejected submission, you can replace the previous receipt by dragging and dropping the updated receipt onto the previously existing one in Expensify.  You do not need to delete and re-create the expense.
</details>

The procedure by which reimbursable expenses are processed varies and is dependent on contributor legal status (e.g. independent contractor, employee) and [subsidiary assignment](https://about.gitlab.com/handbook/tax/#tax-procedure-for-maintenance-of-gitlabs-corporate-structure). Check with Payroll if you are unsure about either of these.

For general information and guidelines regarding the company expense policy, check out the section of our team handbook on [Spending Company Money](/handbook/spending-company-money). Managers and Accounts Payable team will review the expenses for compliance with the company travel policy.  The CEO will review selected escalations at least annually.

Team members should also consider the terms and conditions of their respective contractor agreements when submitting invoices to the company.

Team members in a US policy will be automatically reimbursed through Expensify after their report is "final approved" within 7 business days by Accounts Payable team. For all other team members, please see the reimbursement process below based on your location or employment status.

## Office Equipment and Supplies

The company will reimburse for the following items if you **need it for work or use it mainly for business**, and local law allows us to pay for it without incurring payroll taxes. Please keep in mind that while the amounts below are guidelines and not strict limits, any purchase (other than a laptop) that will cost GitLab $1000 USD per item (or over) will require prior approval from your Manager and Accounting.

The below averages are what GitLab **usually** reimburses. If you prefer to spend more on a given item, that's OK considering the average price. You are also welcome to expense the `GitLab Average Price in USD` portion, and cover the rest with personal funds. For example, if you purchase a high-end Steelcase ergonomic chair, you are welcome to expense the average price per the table below and cover the rest with personal funds. We are encouraging our members to use their best judgment when spending the company's money.

When reimbursing through Expensify please select "reimbursable" for the portion which GitLab will be covering, and add a note explaining the receipt difference.

* Please note Expensify cannot sync "non-reimbursable" line items, only reimbursable line items are to be entered in Expensify.

Team members should not use a Corporate Credit Card to purchase office equipment for their personal workspace.  All office equipment purchases for a team member's personal workspace should be made on a personal credit card and expensed.

For Laptop Purchases/Refreshes, please refer to [IT Ops Laptop](/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptops) policy and procedure.

### Setting up a home office for the first time?

Take inspiration from our [all-remote page covering key considerations for a comfortable, ergonomic workspace](/company/culture/all-remote/workspace/). You can also consult the `#questions` and `#remote` channels in Slack for recommendations from other GitLab team members.

### Software
1. We have central [license management](/handbook/tools-and-tips/other-apps/#jetbrains) for you to
   request licenses for JetBrains' products like RubyMine / GoLand.
1. We do not issue Microsoft Office 365 licenses, as GitLab uses Google Workspace
   ([Docs](/handbook/communication/#google-docs), Slides, Sheets, etc.) instead.
1. For security related software, refer to the security page for [laptop and desktop
   configuration](/handbook/security/#laptop-or-desktop-system-configuration).

### Hardware

It is uncommon for you to need all of the items listed below. Read [GitLab's guide to a productive home office or remote workspace](/company/culture/all-remote/workspace/), and use your best judgement and buy them as you need them. If you wonder if something is common, feel free to ask IT Ops (and in turn, IT Ops should update the list).

| Item                                                                                                  | Average Price in USD | Reimbursable Amount | Importance | Why                                                                                                                                                                                                                                                                                                                                                                   |
|-------------------------------------------------------------------------------------------------------|----------------------|---------------------|------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Height-adjustable desk](/company/culture/all-remote/workspace/#desks)                                | $300 - $500          | $500                | 10/10      | We use our desks everyday and need them to be ergonomic and adjustable to our size and needs, whether that is sitting or standing.                                                                                                                                                                                                                                    |
| [Ergonomic chair](/company/culture/all-remote/workspace/#chairs)                                      | $200 - $400          | $400                | 10/10      | We use our desk chairs hours at a time, and need them to be healthy, supportive, and comfortable.                                                                                                                                                                                                                                                                     |
| [Headphones (wired or wireless, with mic ability)](/company/culture/all-remote/workspace/#headphones) | $100                 | $100                | 10/10      | We use our headphones everyday during our [meetings](/company/culture/all-remote/meetings/), to connect with our fellow team members. We need our headphones to be comfortable, functional, and great quality.                                                                                                                                                        |
| [External monitor](/company/culture/all-remote/workspace/#monitors)                                   | $380                 | $380                | 10/10      | Finding a monitor that is large, comfortable to use with sharpness is extremely important for our eyes and health.                                                                                                                                                                                                                                                    |
| [Keyboard](/company/culture/all-remote/workspace/#external-keyboard-and-mouse)                        | $250                 | $250                | 10/10      | Find a keyboard that works for you and is comfortable for your workflow and hand size.                                                                                                                                                                                                                                                                                |
| [Mouse or Trackpad](/company/culture/all-remote/workspace/#external-keyboard-and-mouse)               | $80 or $145          | $145                | 10/10      | Find a mouse/trackpad that works for you and is comfortable for your workflow.                                                                                                                                                                                                                                                                                        |
| Laptop stand                                                                                          | $90                  | $90                 | 10/10      | Your eyes and head must look at the same angle as you work and so your laptop must be elevated if you are working with an external monitor.                                                                                                                                                                                                                           |
| [Webcam](/company/culture/all-remote/workspace/#webcams)                                              | $80                  | $80                 | 9/10       | If you would like a much better image quality than from the camera in your laptop, a webcam can make video conversation better. For those who interface routinely with clients, leads, and external parties, you may also consider a pricier [mirrorless or DSLR camera as a webcam](https://docs.crowdcast.io/en/articles/1935406-how-to-use-your-dslr-as-a-webcam). |
| [Dedicated microphone](/company/culture/all-remote/workspace/#microphones)                            | $130                 | $130                | 6/10       | For those who routinely interface with clients, leads, media, and external parties — or create regular content for GitLab channels — you may also consider a dedicated microphone to capture your voice with added richness and detail.                                                                                                                               |
| Portable 15" external monitor                                                                         | $200                 | $200                | 9/10       | You have the freedom to work from any location, and having a portable monitor allows that your workflow does not suffer from being constrained to a single small laptop screen.                                                                                                                                                                                       |
| USB-C Adapter                                                                                         | $80                  | $80                 | 9/10       | Most MacBooks only have 1 free USB-C port, so an adapter with additional ports is a necessity.                                                                                                                                                                                                                                                                        |
| HDMI / monitor cable                                                                                  | $15                  | $15                 | 9/10       | Find a quality cable so that the connection between your laptop and monitor is healthy and secure.                                                                                                                                                                                                                                                                    |
| [Yubikey](https://www.yubico.com/store/)                                                              | $50                  | $50                 | 8/10       | Per our [Security Practices](/handbook/security/), purchasing Yubikey is not mandatory, but is considered as an extra layer of authentication for better security.                                                                                                                                                                                                    |
| Monitor Privacy Filter                                                                                | $80                  | $80                 | 8/10       | Important if you work in public places and need to be certain your work cannot be seen.                                                                                                                                                                                                                                                                               |
| WiFi Router with guest functionality                                                                  | $80                  | $80                 | 7/10       | If your existing router does not allow for isolating your work notebook from your personal devices in your home network, consider buying a router that does.                                                                                                                                                                                                          |
| Ethernet connector                                                                                    | $20                  | $20                 | 6/10       | This is if you choose to connect to your internet directly versus by Wi-Fi.                                                                                                                                                                                                                                                                                           |
| Powerline / WiFi network extender                                                                     | $50 - $100           | $100                | 6/10       | Useful for extending your home network to your workspace                                                                                                                                                                                                                                                                                                              |
| Laptop bag or backpack                                                                                | $60                  | $60                 | 7/10       | Carry your laptop and external monitor safely with this travel bag. We recommended you get a bag (or backpack) with straps so the device stays on you when you need your hands free.                                                                                                                                                                                  |
| [Earpods](https://www.apple.com/shop/product/MNHF2AM/A/earpods-with-35-mm-headphone-plug)             | $30                  | $30                 | 7/10       | If you use your headphones at home but would like lighter headphones to use while on the go, these earpods are the way to go.                                                                                                                                                                                                                                         |

If you wish to go over the recommended reimbursement, this is an exception and should first be raised with your manager and if supported, should be raised in the "expense-reporting-inquiries who will review and respond to the request.

### Other
1. Business cards ordered from Moo as per the [instructions](https://about.gitlab.com/handbook/people-group/frequent-requests/#business-cards) provided by the People Experience team.
	*Urgent Business cards needed for day of start can be requested by emailing people-exp@gitlab.com. As a last resort, Moo does offer 3 to 4 Day Express service.*
1. Work-related books.

### Transport/Delivery of free procurements
Also feel free to check your local [Freecycle group](https://www.freecycle.org/) or similar second-hand/free markets when looking for equipment, especially furniture such as desks and chairs. GitLab will reimburse the cost of any transport and delivery services you need to procure the item(s) provided the total cost does not exceed the average spend for the item based on the table above.

GitLab will also reimburse costs relevant to one's location in cases where anniversary gifts or company issued swag incurs additional import costs.

### Not sure what to buy?

Look at our [equipment examples page](/handbook/spending-company-money/equipment-examples) to see some of the items that other GitLab team members have purchased, and please consider adding to the list if there's something you'd like to share.

### Reimbursements

The company will reimburse for the following expenses if you need it for work or use it mainly for business, and local law allows us to pay for it without incurring taxes:
1. Mileage is reimbursed according to local law: [US rate per mile](http://www.irs.gov/Tax-Professionals/Standard-Mileage-Rates), [rate per km in the Netherlands](http://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/zakelijk/auto_en_vervoer/auto_van_de_onderneming/autokosten/u_rijdt_in_uw_eigen_auto), or [rate in Belgium](https://fedweb.belgium.be/nl/verloning_en_voordelen/vergoedingen/vergoeding-voor-reiskosten). Add a screenshot of a map to the expense in Expensify indicating the mileage.
1. Internet connection subscription.
	* For employees outside the Netherlands: follow normal expense report process.
	* For employees in the Netherlands: fill in and sign the [Regeling Internet Thuis](https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq) form and send it to the People Experience team at `people-exp@ gitlab.com`. The People Experience team will then send it to the payroll provider in the Netherlands via email. The details of the payroll provider can be found in the PeopleOps vault in 1Password under "Payroll Contacts".
1. VPN service subscription. Please read [Why We Don't Have A Corporate VPN](/handbook/security/#why-we-dont-have-a-corporate-vpn) for more information about VPN usage at GitLab.
1. Mobile subscription, we commonly pay for that if you call a lot as a salesperson or executive, or if your position requires participation in an oncall rotation. If your device cost is part of your monthly subscription cost, please do not include the device cost from the expense reimbursement.
1. Telephone land line (uncommon, except for positions that require a lot of phone calls)
1. Skype/Google Hangouts calling credit (uncommon, since we mostly use [internet-based services such as Zoom](/blog/2019/08/05/tips-for-mastering-video-calls/))

### Coworking or external office  space
If working from home is not practical you may submit for reimbursement for the cost of a co-working space. This can include non-traditional spaces that require a recurring (full-time monthly) membership as long as you average at least ~4 working days per month at the space. If flexible membership options exist in the form of daily passes or hourly packages, then these can be expensed as well, as long as the prorated cost per month does not exceed that of a recurring membership subscription. For instance, if both the monthly subscription and a hypothetical 10-day pass is $200 USD, then you can only expense one such pass each month.

Any agreement must be between the team member and the co-working space (i.e. GitLab will not sign or appear on the agreement). All expenses must be submitted through the normal [travel and expense reimbursement policy](/handbook/finance/accounting/#reimbursable-expenses). The Company will not be responsible for the deposit, and therefore will not reimburse for the upfront deposit. This will solely be the responsibility of the employee. In addition, the company will not be responsible for any expense that relates to office space subsequent to the termination of service between GitLab and the team member.


### Work-related online courses and professional development certifications

GitLab team members are allotted [$10,000 USD](https://www1.oanda.com/currency/converter/) per calendar year to spend on one or multiple training courses. Follow the process for submitting a [Growth and Development Reimbursement](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit).


### Team building Budget

In FY22, each eGroup member has been allocated $50 per team member per quarter for FY22-Q1 to FY22-Q3 for team building events. There is an additional budget for $100 per team member in FY22-Q4. More to come in the section below as we get closer to FY22-Q4.

### Year-end Holiday Party Budget

In previous years, GitLab has allocated [$100 USD](https://www1.oanda.com/currency/converter/) per GitLab team member for a holiday event in December. We also encouraged GitLab team members to self organize holiday parties with those close by, but due to COVID restrictions in most countries, we have decided to have a more universal approach this year. We will update this policy again ahead of July 2021. 

## Travel and Expense Guidelines

### Travel

1. If you are taller than 1.95m or 6'5", you can upgrade to Economy Plus. There is no dollar restriction on this since it will be hard to fit in economy with that height.
1. For flights longer than 8 hours, you can expense:
   * Up to the first [$300 USD](https://www1.oanda.com/currency/converter/) for an upgrade to Business Class on flights longer than 8 hours if you are taller than 1.95m or 6'5".
    * Up to the first [$100 USD](https://www1.oanda.com/currency/converter/) for an upgrade to Economy Plus (no height restriction) on flights longer than 8 hours.
1. GitLab does not cover expenses for significant others or family members for travel or immigration. This includes travel and visas for GitLab events.
	* There are other things that [the company will not reimburse](/handbook/finance/accounting/#8-employee-reimbursements---expensify), such as dog boarding.

For additional Company Travel related questions, please refer to our [Travel Handbook](/handbook/travel/#booking-travel-through-tripactions-) regarding booking travel through TripActions.

### Spend reduction

When reducing spend, we'll not take the easy route of (temporarily) reducing discretionary spending.
Discretionary spending includes expenses like travel, conferences, gifts, bonuses, merit pay increases and Contributes.
By reducing in these areas we put ourselves at risk of [increasing voluntary turnover among the people we need most](https://steveblank.com/2009/12/21/the-elves-leave-middle-earth-–-soda’s-are-no-longer-free/).
Discretionary spending is always subject to questioning, we are frugal and all spending needs to contribute to our goals.
But we should not make cuts in reaction to the need to reduce spend; that would create a mediocre company with mediocre team members.
Instead, we should do the hard work of identifying positions and costs that are not contributing to our goals.
Even if this causes a bit more disruption in the short term, it will help us ensure we stay a great place to work for the people who are here.

### Renting Cars

#### US and Canada

##### Third Party Liability

Purchase the liability insurance that is excess of the standard inclusion of State minimum coverage in the rental agreement at the rental agency. GitLab’s insurance policy provides liability insurance for rental cars while conducting company business, but it may be excess over any underlying liability coverage through the driver or credit card company used to purchase the rental.

Purchase the liability offered at the rental counter if there are foreign employees renting autos in the US or Canada. While workers' compensation would protect an injured US employee, other passengers may have the right to sue. To ensure that GitLab has protection when a foreign employee invites another person into the car we recommend the purchase of this insurance when offered at the rental counter.

##### Physical Damage - Collision Damage Waiver

**Do Not** purchase the Collision Damage Waiver offered at the rental counter. GitLab purchases coverage for damage to rented vehicles.
 If travel to Mexico is required, **purchase** the liability insurance for Mexico offered at the rental counter. You should verify that the rental agreement clearly states that the vehicle may be driven into Mexico and liability coverage will apply.

#### Countries other than the US and Canada

##### Third Party Liability

Purchase the liability insurance offered at the rental counter when traveling outside the US and Canada. Automobile Bodily Injury and Property Damage Liability insurance are required by law in almost every country. Please verify this coverage is included with the rental agreement.

##### Physical Damage - Collision Damage Waiver

Purchase the Collision Damage Waiver or Physical Damage Coverage offered by the rental agency when traveling outside the US and Canada.

In the event of an accident resulting in damage to the rental car, the foreign rental agency will charge the credit card used to make the reservation with an estimated amount of repair costs if insurance is not purchased. If this happens, GitLab does not purchase Foreign Corporate Hired Auto Physical Damage Coverage to reimburse for damages.

### Virtual Meal with GitLab Team member(s)

Currently Suspended

### Something else?

No problem, and consider adding it to this list if others can benefit as well.

1. Customer/Partner Facing Events
    * Gala/Black Tie Events: Tuxedo or Gown Rental, $150-$225 USD per event.
    * The event must be customer specific and the invitation must state black tie only.

## Expense Reimbursement

1. Effective 2019-07-01, all expense reports must be submitted to your manager for approval prior to being sent to Finance for payment.
1. If you are a team member from Nigeria, please submit your expense in your salary invoice (a template can be found [here](/handbook/finance/#invoice-template-and-where-to-send)) with receipts attached to <payroll@gitlab.com>.  Please note, this is a temporary solution while we are transition over to a PEO.
1. If you are a team member and incurred an expense charged in a currency different from the one you use to submit your invoices, use the conversion rates specified in the [global compensation section of the handbook](/handbook/total-rewards/compensation/#exchange-rates). If the expense currency doesn’t exist in that list, refer to the conversion rates in [oanda](https://www.oanda.com/currency/converter/). Make sure to set the expense date in the currency converter form.
1. GitLab uses Expensify to facilitate the reimbursement of your expenses. As part of onboarding you will receive an invitation by email to join GitLab's account. Please set up your account by following the instructions in the invitation.
	* If you are a team member in Spain or France, please submit your expenses through Safeguard in-house expense reimbursement management system and also submit them through Expensify.  Accounts Payable will review, approve, and send the approval of your expense reports in Expensify to your gitlab email address.  You will need to forward the approval email to Safeguard enable for them to process your expense reimbursement via payroll.

	* If you are new to Expensify and would like a brief review, please see [Getting Started](https://community.expensify.com/discussion/7703/getting-started-video)
	* For step by step instructions on creating, submitting, and closing a report please see [Create, Submit, Close](https://docs.expensify.com/en/articles/2921-report-actions-create-submit-and-close)
	* For US team members, the approved expense amount will be deposited into your account a few days after the report has been approved by Accounts Payable.
	* For Australia, New Zealand, Belgium, Germany, and Netherlands, AP will process the approved report on Friday.  The payment will be deposited into your account no later than three business days the following week. 
    Accounts payable uses Tipalti to drive payment to the above policies. As part of onboarding, an invitation to the Tipalti portal will be sent. Please sign up and onboard only banking information, no tax information is needed. If set up of the personal Tipalti account is not completed in a timely manner, this may result in a delay of expense payment.

	* For all team members being pay by Safeguard, iiPay, or Vistra, the approved expense amount will be deposited in your account with your monthly salary.

1. If you are a team member with a company credit card, your company credit card charges will automatically be fed to a new Expensify report each month. Please attach receipts for all expenses (per the Expense Policy, see below) within 1 business days after the end of the month. These amounts will not be reimbursed to you but Expensify provides a platform for documenting your charges correctly.

### Reimbursement process and timeline:

##### SafeGuard
Team members who are employed through SafeGuard must submit their expense for reimbursement through Expensify.  All expense reports must be submitted and approved by the manager and the Accounts Payable team by the 8th of the month to be included in the current month payment.

Team members in France, Italy, and Spain must submit their expenses through:
*  Expensify
*  Safeguard in-house expense reimbursement
*  GitLab payroll send the expense approval to Safeguard after the team member's manager approved the report
*  Team members send the original receipts to Safeguard

##### Global Upside & Remote.com

* Team members must submit their expenses through Expensify. All expense reports must be submitted and approved by manager and the Accounts Payable team by the 8th of the month to include in the current month payment.

##### iiPay

* All Individual contractors or C2C, with exception of Nigeria will be reimbursed by iiPay by the 22nd of each month.  All expense reports must be approved by the manager and the Accounts Payable team by the 8th of each month to be include in the current month payment.  For contractor with C2C status, be sure to contact Payroll team via email at nonuspayroll@gitlab.com and ap@gitlab.com if you need to set up a separate bank for your expense reimbursement.

##### Legal entities

* Expense reports for GitLab Ltd (UK) must be approved by the manager and the Accounts Payable team on or before the 14th of each month in order for the reimbursement to be include in the current month payroll.
* Expense reports for GitLab Canada Corp must be approved by the manager and the Accounts Payable team before the 1st day of each payroll period.  Please see [Payroll Calendar](https://docs.google.com/spreadsheets/d/1ECkI_Z8R82j1eipJEEybXjO-EDtzw4TuhJPOnHypDho/edit#gid=0) for the payroll cut off date.
* Expense reports for GitLab BV (Belgium and Netherlands), GmbH (Germany), PTY Ltd (Australia and New Zealand) are reimbursed via GitLab AP within 10 business days from the approval date by their manager and the Accounts Payable team.
* Expense reports for GitLab Inc, GitLab Inc Billable, and GitLab Federal reimbursed via Expensify, and AP will final approve the report within 5 business days after the approval from their manager.

##### Nigeria

* Please include your expenses along with receipts on your monthly salary invoice.

##### CXC Global

Poland, Russia, Ukraine, Romania, Israel, Turkey, Czech Republic & Portugal:

* Expense reports must be submitted on Expensify by team members, approved by their managers, and final approved by Accounts Payable team on or before the 8th of each month to ensure it is included in the current months payroll
* GitLab Payroll will send the approved expense amount to CXC EMEA payroll to include with the monthly salary
* Team members must include the approved expense amount on their monthly invoice as well.

## Expense Policy

1. Max Expense Amount - [$5,000 USD](https://www1.oanda.com/currency/converter/)  - NOTE - If you are a corporate credit card holder, please refer to the [corporate credit card policy section](/handbook/finance/accounting/#credit-card-use-policy) for those specific instructions and thresholds.
1. Itemized receipts are required for all expenses.
1. Expenses should be submitted within 30 days. This helps the Company to better manage our budget and ensures accurate month-end reporting.
1. Expense report items should clearly state if the spend includes amounts for clients / non-team members.  Tax requirements in certain countries require us to account for client spend differently.
1. All team members must submit their expense reports in their designated policies in Expensify - COGS team members in COGS policies and non-COGS team members in non-COGS policies.
1. Giftcards are not accepted as a form of payment for business expenses.
1. All internet/telephone reimbursements must have a detailed receipt attached which shows the amount, service, and service date.

* Please note we do not reimburse late fees or equipment costs.

## Team Member Expense Temporary Advances

These instructions apply if a team member is unable to purchase required items, for whatever reason.

1. The team member will make a list of requested items and prices, noting if they are out of the budget range listed in the [Expenses handbook section](/handbook/finance/expenses/) (if applicable), and send it to their manager for approval. We ask that only one list be sent, versus multiple lists.
1. The team member's manager will send the approved (or edited) list to Accounting (nonuspayroll@gitlab.com OR uspayroll@gitlab.com, and CC ap@gitlab.com) for final approval and dispensation.
1. Once approved, Payroll will send the team member an invoice template to fill out with the approved items, prices and the team member's bank information.
1. The approved final amount will be sent to the team member's bank and they can then purchase their approved items.
1. Receipts should be submitted to ap@gitlab.com after the purchases have been made.

## Approving Expense Reports

1. All expense reports are approved by the team members direct manager or their designated approver when they are out of the office.
1. It is expected that the expense report approver will perform a complete review to ensure the reasonableness and accuracy of the submitted expenses.
1. Expensify will send a notification email to the designated approver when a team member submits an expense report.
    * Click on the report name in the body of the email
    * Review each expense for the correct amount of the receipt and the report
    * Check for customers or project name if applicable under Tag
    * We required a receipt for any expense greater than a $5 cash purchase (except for Billable policy)
    * Select [Approve and Forward] option and Expensify pre-populated the email address.  Note, Expensify is updating their coding to address a small glitch in this field.  If it is empty, please send it to **Montpac** (gitlab-expensify-mp@montpac.com)
    * **Important** - please do not use [Final Approval] because Expensify will not send the email notification for payment approval and it will delay the reimbursement process
    * Manager can delegate the approval process during PTO:
        *  Settings
        *  Your Account
        *  Vacation Delegate
        *  Enter the email address of the backup approval
    * All expense question(s) can be addressed via expenses@gitlab.com or in the #Finance and #expense-reporting-inquires
Slack channel

1.  **Expenses Reports approval deadline**
    * Australia, Germany, New Zealand, Netherlands, United States - as soon as possible
    * United Kingdom - all expense reports must be approved by manager no later than the 14th of each month.  Team members - please be sure to submit your report(s) couple days before the due date so your manager has enough time for approval.
    * Canada - before 1st day of payroll period
    * All non-US contractors - all expense reports must be approved by manager no later than the 8th of each month. Team members - please be sure to submit your report(s) couple days before the due date.

## Non-reimbursable Expenses:

Examples of things we have not reimbursed:
1. Costume for the party at the end of Contribute.
1. Boarding expense for animals while traveling.
1. Headphones costing $800 which were found to be in excess of our standard equipment guidelines.
1. Batteries for smoke detector.
1. Meals during the Contribute when team members opt out of the company provided meal option.
1. Cellphones and accessories.
1. Travel related expenses for family members of GitLab employees.
1. Fitness equipment (treadmill, etc..) and gym membership.
1. Meals from co-working day(s).
1. Home office decor (picture frames, hanging plants, area rugs, book shelves)
1. Meals or beverages while attending co working space
1. Treadmill Desks
1. Portable Air Conditioners

In accordance with [Reimbursable Expense guidelines](/handbook/finance/accounting/#reimbursable-expenses), independent contractors should note which expenses are Contribute related on their invoices, prior to submitting to the Company.

----

Return to the main [finance page](/handbook/finance/).

