---
layout: markdown_page
title: "Product Direction - Distribution"
description: "The Distribution group's mission is to set companies up for success by making it easy to deploy, maintain, and update a self-managed GitLab instance"
canonical_path: "/direction/distribution/"
---

- TOC
{:toc}

## Overview
GitLab is the engine that powers many software businesses. In a world where success requires developers to move quickly, it is
important to ensure that end users can accomplish their work as quickly as possible, with minimal interruptions, and they have access to the latest and greatest that GitLab has to offer. The Distribution group's mission is to set companies up for success by making it easy to deploy, maintain, and update a self-managed GitLab instance and ensure it is highly available for their users. 

## Category

### Omnibus

Today we have a mature and easy to use Omnibus based build system, which is the
most common method for deploying a self-managed instance of GitLab. It includes everything a
customer needs to run GitLab all in a single package, and is great for
installing on virtual machines or real hardware. We are committed to making our
package easier to work with, providing a first-class solution for database fault tolerance, and improving the [zero-downtime upgrade](https://docs.gitlab.com/omnibus/update/#zero-downtime-updates) experience.

[Category Vision](/direction/distribution/omnibus/) &middot; [Documentation](https://docs.gitlab.com/omnibus/)

### Cloud-native installation
We also want GitLab to be the best cloud native development tool, and offering a
great cloud native deployment is a key part of that. We are focused on offering
a flexible and scalable container based deployment on Kubernetes and OpenShift.


The Helm charts are currently considered to be at the [Viable maturity level](https://about.gitlab.com/direction/maturity/), are we starting work on an [operator](https://gitlab.com/gitlab-org/gl-openshift/gitlab-operator). These epics will be used to define what it will take to get to the next maturity levels and track the work to be done:

[Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2260)

[Viable to Lovable](gitlab-org/charts/gitlab#1658)

[Category Vision](/direction/distribution/cloud_native_installation/) &middot; [Documentation](https://docs.gitlab.com/charts/)